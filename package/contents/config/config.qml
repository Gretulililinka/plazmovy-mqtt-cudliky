//tady sou definovaný taby v konfiguraci widgetu naploše
import QtQuick 2.0

import org.kde.plasma.configuration 2.0 as PlasmaConfig

PlasmaConfig.ConfigModel
{
    PlasmaConfig.ConfigCategory
    {
        name: i18n('Nastavování čudliků')
        icon: 'preferences-desktop-plasma'
        source: 'NastavovaniCudliku.qml'
    }

}

 
