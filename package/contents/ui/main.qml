import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11

//import org.kde.quickcharts 1.0 as Charts
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.components 3.0 as PlasmaComponents

// naimportujem si všecky voběkty ze složšky 'cudliky'
import "./cudliky"

// taky si naimportujem náš c++/qt plugin
import org.kde.private.mqttCudliky 1.0

//chceme takovej ten velkej widget naplochu
Item
{
    // podporujem jenom velkej widget strčenej naplochu
    Plasmoid.preferredRepresentation: Plasmoid.fullRepresentation
    
    Plasmoid.fullRepresentation: Item
    {
        id: main
        
        // minimální šířka/vejška widgetu pole počtu čudliků
        Layout.minimumWidth: (velikostMezer + 64) * main.sloupcu * PlasmaCore.Units.devicePixelRatio
        Layout.minimumHeight: (velikostMezer + 64) * main.radku * PlasmaCore.Units.devicePixelRatio
        
        // počet sloupců a řádků grid/mřížškovýho layoutu/rozložení čudliků
        // čudliky se budou strkat dotoho elmentu grid
        property alias radku: grid.rows
        property alias sloupcu: grid.columns
        
        // velikost mezer mezi čudlikama v pixelech
        property int velikostMezer: 2
        
        // různý pole který držej referenci na voběkty čudliků
        
        // uplně všecky čudliky
        property var vseckyCudliky: []
        
        // čudliky podle jednotlivejch topiců
        // key/klíč pole sou názvy topiců
        property var cudlikyPodleTopicu: []
        
        // taky čudliky podle topiců ale takový co dělaj že chtěj psaníčko v *.json formátu :D ;D
        property var jsonCudlikyPodleTopicu: []
        
        // seznam jenom těch čudliků co uměj publikovat
        // potřebujem znát nato abysme je mohli dycky všecky zamknout když mqtt klient ztratí spojení s brokerem
        property var publikovaciCudliky: []
        
        
        // 'sada' všech topiců s nejvíc nejvěčím QoS jako hodnotou
        // potřebujem ji mit jako vobyč array/pole abysme ji mohli strčit do qt/c++
        // v seznamTopicu by měli bejt jenom stringy a v qos zase samý int
        // ( de ňák v js hezky rozlousknout pole na dvě různý žeby v jednom byly keys a vdruhým values?????? nějakej takovej vopak zip :O :O )
        property var seznamTopicu: []
        property var seznamTopicuQoS: []
        
        // výchozí barvy když nejsou definovaný v konfigu
        property color barvaTextu: 'black'
        property color barvaAktivni: 'red'
        property color barvaKontury: 'green'
        property color barvaPozadi: 'white'
        
        // načtem ůdaje o brokeroj/přihlašovací ůdaje z konfigu
        // mužou bejt i undefined/nedefinovaný to se pak hlídá dál aby se nezačalo připojovat
        property string adresa: plasmoid.configuration.adresa
        property string id: plasmoid.configuration.id
        property string uzivatel: plasmoid.configuration.uzivatel
        property string heslo: plasmoid.configuration.heslo
        
        // výchozí konfigurační string pro případ že neni žádnej uloženej v konfigu
        
        // normálně by asi jako měla fungovat hodnota strčená mezi <default></default> tagy v 
        // config.xml ale ňák ji to nechtělo načítat :O :/
        // možná to neumí zhamat *.json možná dělaj problémy uvozovky možná to nefunguje
        // jenom v tom testovacím plasmoidviewer vokýnku. nevim :O :/
        
        property string jsonString: 
        '{

        "grid":
            {
                "sirka": 1,
                "vyska": 1
            },
        "barvy":
            {
                "pozadi": "#0f808080",
                "text": "#d3d3d3",
                "aktivni": "#00ffff",
                "kontura": "#191919"
            },
        "cudliky": [
            {
                "druh": "Moznosti",
                "nazev": "čudlík",
                "muzePublikovat": false,
                "muzePrijimat": false,
                "x": 0,
                "y": 0,
                "topic": "nejaky_topic",
                "seznamMoznosti": ["vychozi nastaveni cudliku"]
            }
            ]
        }'

        // funcke co dělá že veme json a podle něj vyrobí všecky čudliky
        // nastaví barvičky/velikost mřížšky a takový tydlety
        // taky nám naplní všecky ty pole čudliků
        function jsonNaElementy(json)
        {

            // nastavení počtu řádků/sloupců mřížšky
            main.sloupcu = json.grid.sirka;
            main.radku = json.grid.vyska;
            
            // jestli jsou v json barvičky tak nastavíme barvičky :D ;D
            if(json.hasOwnProperty('barvy'))
            {
                barvaAktivni = json.barvy.aktivni;
                barvaTextu = json.barvy.text;
                barvaPozadi = json.barvy.pozadi;
                barvaKontury = json.barvy.kontura;
            }
            
            // noa teďko projdem všecky čudliky v json a zkusíme je dynamicky vyrobit a nastrkat do tý mřížšky
            for(var i in json.cudliky)
            {
                // vezmem si z json další json samotnýho jednoho čudlika
                var cudlikJson = json.cudliky[i];
                
                // id našeho čudliku vyrobenýho
                // nějaký id asi musíme vyplnit jinak nemá nějakej víc věčí význam
                var id = 'cudlik_' + i;

                // čudlik budem vyrábět funkcí 'createQmlObject' která má navstupu string kterej vobsahuje zdrojáček qml elementu
                // náš string začnem importem
                var obektStr = 'import "./cudliky";';
                obektStr += cudlikJson.druh + '{';
                
                // jestli muže čudlik publikovat tak taky musí mit možnost posílat publikovací signál dalším qt/qml elementům
                // bude posílat signál s topicem a nějakou tamtou zprávou
                if(cudlikJson.muzePublikovat)
                {
                    obektStr += 'signal zpravaOdCudliku(string topic, string zprava, int qos, bool retain);';
                }
                obektStr += '}';
                
                // vyrobíme si čudlik strčíme ho do gridu a nastavíme mu nějaký id
                var cudlik = Qt.createQmlObject(obektStr, grid, id);
                
                // další atributy čudliku nastavíme jako atributy javascriptovýho voběktu
                
                // méno a topic ke kterýmu je připojenej
                cudlik.nazev = cudlikJson.nazev;
                cudlik.mqttTopic = cudlikJson.topic;
                
                // čudlik bude v mřížšce vyplňovat všecky přidělený chlívečky
                cudlik.Layout.fillHeight = true;
                cudlik.Layout.fillWidth = true;
                
                // nastavíme pozici v mřížšce/gridu
                cudlik.Layout.column = cudlikJson.x;
                cudlik.Layout.row = cudlikJson.y;
                
                // jestli má čudlik v json nastavenou nějakou šířku/výšku teda jakože kolik
                // chlívečků mřížky má našířku/navejšku tak nastavíme
                if(cudlikJson.hasOwnProperty('sirka'))
                    cudlik.Layout.columnSpan = cudlikJson.sirka;
                if(cudlikJson.hasOwnProperty('vyska'))
                    cudlik.Layout.rowSpan = cudlikJson.vyska;
                
                // vobarvení čudliku
                // jinak má svý nějaký výchozí barvičky
                cudlik.barvaAktivni = barvaAktivni;
                cudlik.barvaTextu = barvaTextu;
                cudlik.barvaPozadi = barvaPozadi;
                cudlik.barvaKontury = barvaKontury;
                
                // jestli čudlik muže publikovat tak mu nastavíme QoS/kvalitu služby posílací parametr + retain 
                // a taky napojíme jeho signál 'zpravaOdCudliku' na funkci 'zpravaCudliku'
                if(cudlikJson.muzePublikovat)
                {
                    cudlik.mqttQos = cudlikJson.hasOwnProperty('QoS') ? cudlikJson.QoS : parseInt(0);
                    cudlik.mqttRetain = cudlikJson.hasOwnProperty('retain') ? cudlikJson.retain : false;
                    
                    cudlik.zpravaOdCudliku.connect(zpravaCudliku);
                }
                else
                {
                    cudlik.muzePublikovat = false;
                }
                
                // jestli máme v json definovanej nějakej atribut 'jsonElement' jakože čudlik
                // chce vezprávičkách dycky luštit nějakej json a zněj si něco vyzobnout tak nastavíme že parsuje json
                if(cudlikJson.hasOwnProperty('jsonElement'))
                {
                    cudlik.parsujeJson = true;
                    cudlik.jsonElement = cudlikJson.jsonElement;
                }
                
                // jestli pracuje/drží datovej typ v celočíselný podobě nebo ne
                if(cudlikJson.hasOwnProperty('jeCelociselny'))
                {
                    cudlik.jeCelociselny = cudlikJson.jeCelociselny;
                }
                
                // jestli má titulek nebone
                if(cudlikJson.hasOwnProperty('maTitulek'))
                {
                    cudlik.maTitulek = cudlikJson.maTitulek;
                }
                
                // ruzný specifický atributy pro různý druhy čudliků
                switch(cudlikJson.druh)
                {
                    
                    case 'Prepinadlo':
                        if(cudlikJson.hasOwnProperty('zpravaPriOn'))
                            cudlik.zpravaPriOn = cudlikJson.zpravaPriOn;
                        if(cudlikJson.hasOwnProperty('zpravaPriOff'))
                            cudlik.zpravaPriOff = cudlikJson.zpravaPriOff;
                        if(cudlikJson.hasOwnProperty('onText'))
                            cudlik.onText = cudlikJson.onText;
                        if(cudlikJson.hasOwnProperty('offText'))
                            cudlik.offText = cudlikJson.offText;
                        
                        // aktualizace přepínadla protože vokamžitě nemá barvičku
                        cudlik.aktualizovatCudlik();
                        break;
                        
                    case 'Soupatko':
                    case 'Kruh':
                        if(cudlikJson.hasOwnProperty('minHodnota'))
                            cudlik.hodnotaMin = cudlikJson.minHodnota;
                        if(cudlikJson.hasOwnProperty('maxHodnota'))
                            cudlik.hodnotaMax = cudlikJson.maxHodnota;
                        if(cudlikJson.hasOwnProperty('znakyZaHodnotou'))
                            cudlik.znakyZaHodnotou = cudlikJson.znakyZaHodnotou;
                        
                        console.log(cudlik.hodnotaMin);
                        console.log(cudlikJson.hodnotaMin);
                        
                        if(cudlikJson.druh == 'Kruh')
                            cudlik.aktualizovatPrumerTakyStred();
                        
                        break;
                        
                    case 'Graf':
                        if(cudlikJson.hasOwnProperty('pocetCarMrizky'))
                            cudlik.pocetCarMrizky = cudlikJson.pocetCarMrizky;
                        break;
                    case 'Tlacitko':
                        if(cudlikJson.hasOwnProperty('coPosila'))
                            cudlik.coPosila = cudlikJson.coPosila;
                        if(cudlikJson.hasOwnProperty('napis'))
                            cudlik.napis = cudlikJson.napis;
                        break;
                        
                    case 'Moznosti':
                        if(cudlikJson.hasOwnProperty('seznamMoznosti'))
                            cudlik.seznamMoznosti = cudlikJson.seznamMoznosti;
                        break;
                        
                    case 'Napis':
                        if(cudlikJson.hasOwnProperty('maOkraj'))
                            cudlik.maOkraj = cudlikJson.maOkraj;
                        break;
                    
                    case 'Kompas':
                    case 'PolarniGraf':
                        if(cudlikJson.hasOwnProperty('maLegendu'))
                            cudlik.maLegendu = cudlikJson.maLegendu;
                        
                        if(cudlikJson.hasOwnProperty('legendaTop'))
                            cudlik.legendaTop = cudlikJson.legendaTop;
                        if(cudlikJson.hasOwnProperty('legendaRight'))
                            cudlik.legendaRight = cudlikJson.legendaRight;
                        if(cudlikJson.hasOwnProperty('legendaDown'))
                            cudlik.legendaDown = cudlikJson.legendaDown;
                        if(cudlikJson.hasOwnProperty('legendaLeft'))
                            cudlik.legendaLeft = cudlikJson.legendaLeft;
                        
                        if(cudlikJson.hasOwnProperty('pocetKroku'))
                            cudlik.pocetKroku = cudlikJson.pocetKroku;
                        
                        if(cudlikJson.druh == 'PolarniGraf')
                        {

                            if(cudlikJson.hasOwnProperty('maxDylkaFronty'))
                                cudlik.maxDylkaFronty = cudlikJson.maxDylkaFronty;
                            if(cudlikJson.hasOwnProperty('klicHodnot'))
                                cudlik.klicHodnot = cudlikJson.klicHodnot;
                            if(cudlikJson.hasOwnProperty('klicUhloveHodnoty'))
                                cudlik.klicUhloveHodnoty = cudlikJson.klicUhloveHodnoty;
                            
                            cudlik.parsujeJson = true;
                            
                        }
                        
                        break;
                        
                    default:
                        console.error('neznamej druch cudliku v json souboru :O :O');
                        break;
                }
                
                
                // přijímací čudliky si rozškatulkujem do polí(slovníků) kde klíčem je topic danýho čudliku dycky
                // nebudem si pak muset přestrkávat zprávu vod mqtt klienta do všech čudliků najednou ale
                // jenom do čudliků danýho topicu
                // máme dvě takový pole polí přijímacích čudliků jedno vobyčejný a druhý pro json
                if(cudlikJson.muzePrijimat)
                {
                    if(cudlik.parsujeJson)
                    {
                        if(cudlikJson.topic in jsonCudlikyPodleTopicu)
                            jsonCudlikyPodleTopicu[cudlikJson.topic].push(cudlik);
                        else
                            // jestli neni tak přidáme nový pole
                            jsonCudlikyPodleTopicu[cudlikJson.topic] = [cudlik];
                    }
                    else
                    {
                        if(cudlikJson.topic in cudlikyPodleTopicu)
                            cudlikyPodleTopicu[cudlikJson.topic].push(cudlik);
                        else
                            cudlikyPodleTopicu[cudlikJson.topic] = [cudlik];
                    }
                }
                else
                {
                    cudlik.muzePrijimat = false;
                }

                // strčíme čudlik do pole který má referenci na všecky čudliky
                vseckyCudliky.push(cudlik);
                
                // jestli čudlik muže publikovat tak taky skováme do pole publikovacích čudliků
                if(cudlik.muzePublikovat)
                    publikovaciCudliky.push(cudlik);
                
                // nakonec skusíme přidat QoS/topic do tý pomyslný 'sady' ze dvou polí
                // topic přidáváme jenom když eště v poli neni. když je tak kouknem jestli je přidávaný qos věčí než to uložený
                // a jestli jako jo tak přepišem
                if(seznamTopicu.includes(cudlikJson.topic))
                {
                    var index = seznamTopicu.indexOf(cudlikJson.topic);
                    if(seznamTopicuQoS[index] < cudlikJson.QoS)
                        seznamTopicuQoS[index] = cudlikJson.QoS;
                }
                else
                {
                    // jestli topic eště v 'sadě' neni tak prostě jako přidáme
                    seznamTopicu.push(cudlikJson.topic);
                    seznamTopicuQoS.push(cudlikJson.QoS);
                }

            }
            
        }
        
        // funkce co vodstraní všecky čudliky
        function odstranitCudliky()
        {
            vseckyCudliky.forEach(cudlik => {cudlik.destroy();});
            vseckyCudliky = [];
            
            publikovaciCudliky = [];
            cudlikyPodleTopicu = [];
            jsonCudlikyPodleTopicu = [];
            
            // seznam topiců promažem taky
            seznamTopicu = [];
            seznamTopicuQoS = [];
        }
        
        // funcke co podle topicu strčí nějakou zprávu všem čudlikům 
        function strcitZpravuCudlikum(topic, obsah)
        {
            console.log('zprava cudlikum topic: ' + topic);
            cudlikyPodleTopicu[topic].forEach(cudlik => {cudlik.prijmout(obsah);});
        }
        
        // tosamý co 'strcitZpravuCudlikum' ale s json
        function strcitJsonZpravuCudlikum(topic, obsah)
        {
            var json = null;
            try
            {
                json = JSON.parse(obsah);
            }
            catch(e)
            {
                console.error('chyba pri parsovani json zpravy: ' + e.name + ', ' + e.message);
            }
            
            if(json != null)
            {
                console.log('json zprava cudlikum topic: ' + topic);
                jsonCudlikyPodleTopicu[topic].forEach(cudlik => {cudlik.prijmoutJson(json);});
            }
        }
        
        // zpráva vod čudliku
        // klient ji zkusí poslat (stav připojení si hlídá metoda 'poslat')
        function zpravaCudliku(topic, obsah, qos, retain)
        {
            console.log('posilam str: '+obsah);
            Klient.poslat(topic, obsah, qos, retain);
        }
        
        // callbacky na vodpojení/připojení/chybu klienta
        // vypnem posílací čudliky
        function odpojeniKlienta()
        {
            malejTextDole.text = 'odpojeno (posílací čudliky teďko nefungujou)';
            publikovaciCudliky.forEach(cudlik => {cudlik.muzePublikovat = false;});
        }
        
        // zapnem posílací čudliky
        function pripojeniKlienta()
        {
            malejTextDole.text = 'připojeno';
            publikovaciCudliky.forEach(cudlik => {cudlik.muzePublikovat = true;});
        }
        
        function chybaKlienta(popis)
        {
            if(popis === undefined)
                malejTextDole.text = 'chyba :D';
            else
                malejTextDole.text = popis;
        }
        
        // funkce co se zavolá jakmile máme qml voběkt kompletní
        Component.onCompleted:
        {

            // napojení signálů z mqtt klienta na vodpovídající funkce tady
            Klient.zpravaDoFrontendu.connect(strcitZpravuCudlikum);
            Klient.zpravaJsonDoFrontendu.connect(strcitJsonZpravuCudlikum);
            
            Klient.odpojenej.connect(odpojeniKlienta);
            Klient.pripojenej.connect(pripojeniKlienta);
            Klient.chyba.connect(chybaKlienta);
            
            // jestli máme v kongiguraci uloženej string json souboru tak ho vemem jinak použijem ten výchozí
            var konfiguracniJsonStr = plasmoid.configuration.konfiguracniJson || jsonString;
            
            // zkisíme json naparsovat
            var json = null;
            try
            {
                json = JSON.parse(konfiguracniJsonStr);
            }
            catch(e)
            {
                console.error('chyba pri parsovani konfiguracniho *.json: ' + e.name + ', ' + e.message);
                malejTextDole.text = 'nešlo naparsovat json :O :O';
            }
            
            if(json != null)
            {
                // jestli máme validní json tak podle něj mužem vyrobit čudliky :D ;D
                jsonNaElementy(json);
                
                // nastavíme seznam 
                Klient.nastavitSeznamTopicu(seznamTopicu, seznamTopicuQoS);
                
                // jestli máme nastavenou brokera/přihlašovací ůdaje a klient neni puštěnej tak ho nastavíme a zapnem
                if(adresa && id && uzivatel && heslo)
                {
                    if(!Klient.jePripojenej())
                    {
                        Klient.nastavit(adresa, id, uzivatel, heslo);
                        if(!Klient.pripojit())
                        {
                            // text chyby napiše do malýho stavovýho textíku dolu ta 'pripojit()' děláním signálů
                            console.error('nepodarilo se pripojit klienta :O :O');
                        }
                    }
                    else
                    {
                        // sem by jsme se jako vubec neměli dostat :D
                        console.log('mqtt klient je asi jako uz pripojenej :O :O');
                    }
                }
                else
                {
                    var txt = 'mqtt klient se nespustil protoze neni nakonfigurovanej';
                    console.log(txt);
                    malejTextDole.text = txt;
                }
            }
            
        }
        
        // když odstraníme widget tak taky jako musíme vipnout mqtt klienta ;D
        Component.onDestruction:
        {
            Klient.odpojit();
        }
        
        
        //regování na změnu konfigurace
        Plasmoid.onUserConfiguringChanged:
        {
            var konfiguracniJsonStr = plasmoid.configuration.konfiguracniJson || jsonString;
            var json = null;
            try
            {
                json = JSON.parse(konfiguracniJsonStr);
            }
            catch(e)
            {
                console.error('chyba pri parsovani konfiguracniho *.json: ' + e.name + ', ' + e.message);
                malejTextDole.text = 'nešlo naparsovat json :O :O';
            }
            
            if(json != null)
            {
                odstranitCudliky();
                jsonNaElementy(json);
                Klient.nastavitSeznamTopicu(seznamTopicu, seznamTopicuQoS);
                
                if(adresa && id && uzivatel && heslo)
                {
                
                    // jestli klient už běží tak mu resetnem
                    // to znamená že ho vodpojíme přepišem parametry a připojíme znova
                    if(Klient.jePripojenej())
                    {
                        Klient.reset(adresa, id, uzivatel, heslo, seznamTopicu, seznamTopicuQoS);
                    }
                    else
                    {              
                        // jinak normálně pustíme
                        Klient.nastavit(adresa, id, uzivatel, heslo);
                        if(!Klient.pripojit())
                        {
                            // text chyby napiše do malýho stavovýho textíku dolu ta 'pripojit()' děláním signálů
                            console.error('nepodarilo se pripojit klienta :O :O');
                        }
                    }
                
                }
                else
                {
                    // jestli je konfigurace špatná/nějakej údaj chybí tak se klient nezačne připojovat
                    var txt = 'mqtt klient se nespustil protoze neni nakonfigurovanej';
                    console.log(txt);
                    malejTextDole.text = txt;
                }
                
            }
        }
        
        
        /*
        *  TODO 
        * todleto se nikdy nezavovlalo :O :O
        *  kolibáč by si tam naně měl pořádně došlápnout aby to tam dali dopořádku si myslim :O :O
        * 
        */
        Plasmoid.onFormFactorChanged:
        {
            console.log("zmena rozliseni");
        }
        
        Plasmoid.onAvailableScreenRegionChanged:
        {
            console.log("zmena!!!!!!!!!!");
        }
        
        Plasmoid.onLocationChanged:
        {
            console.log("zmena222!!!!!!!!!!");
        }
       /* **/
        
        Plasmoid.onAvailableScreenRectChanged:
        {
            console.log("zmena!!!!!!!!!!");
            
        }
        //PLasmoid.onAvailableScreenRegionChanged:
        //{
            //console.log("zmena2!!!!!!!!!!");
            
        //}
        
        
        // grid/mřížka do který se nastrkaj jednotlivý čudliky
        GridLayout
        {
            id: grid
            width: parent.width
            height: parent.height - malejTextDole.height
            rowSpacing: velikostMezer
            columnSpacing: velikostMezer
        }
        
        // malej stavovej textík dole u spodního vokraje widgetu
        // řiká cose právě děje a jestli je widget připojenej/nepřipojenej a takový věci
        Text
        {
            y: grid.height
            id: malejTextDole
            text: 'právě teďko zapnutý'
            color: main.barvaTextu
        }
        
    }

}
