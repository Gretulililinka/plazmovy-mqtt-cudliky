import QtQml 2.2
import QtQuick 2.15

// takovej jakože hodně moc vobyčejnej čudlik kterej dělá jenom to že ukazuje text nějakej chycenej

Cudlik
{
    id: napisRoot
    
    nazev: "napis"
    titulek.text: nazev
    
    property alias text: napisElement.text
    
    // jestli má nějaký pozadí/vokraj 
    // když ne ukazuje se jenom samotnej textík přímo na widgetu
    property bool maOkraj: true
    
    function prijmout(zprava)
    {
        if(!muzePrijimat)
            return;
        text = zprava;
    }

    Rectangle
    {
        id: okraj
        width: parent.width
        height: maTitulek ? parent.height - titulek.height : parent.height
        radius: zakulaceni
        y: maTitulek ? titulek.height : 0

        color: barvaPozadi
        border.color: barvaKontury
        border.width: velikostOkraje
                
        visible: maOkraj
    }
        
    Text
    {
        id: napisElement
        text: "napis"
        width: okraj.width - velikostOkraje * 2
        height: okraj.height - velikostOkraje * 2
        anchors.centerIn: okraj
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        color: barvaTextu
                
        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit

    }
    
    


}
