import QtQml 2.2
import QtQuick 2.9

Cudlik
{
    id: kompas
    
    nazev: "Kompas"
    
    // minimální maximální hodnota aktuální hodnota a možnej rosah hodnoty
    property real hodnotaMin: 0
    property real hodnotaMax: 360
    property real rosahHodnot: hodnotaMax - hodnotaMin
    property real pocetKroku: 36
    
    property real hodnota: hodnotaMin
    
    property string klicHodnot: 'bla'
    property string klicUhloveHodnoty: 'bla'
    
    property bool maLegendu: true
    property string legendaTop: 'top'
    property string legendaDown: 'down'
    property string legendaLeft: 'left'
    property string legendaRight: 'right'
    property real offsetLegendy: 5
    
    titulek.text: nazev
    property real sirkaHlavniCary: 4
    property real sirkaVedlejsiCary: 2
    
    property color barvaVyplneni: Qt.rgba(barvaAktivni.r, barvaAktivni.g, barvaAktivni.b, barvaAktivni.a * 0.5)

    // souřadnice středu toho kompasu a jeho poloměr
    // jeto vymyšlený tak že když je titulek kračí než zdálenost středu kompasu vod levýho/pravýho vokraje tak
    // se kompas do volnýho místa nafoukne. jinak je menčí než titulek a stčenej vo velikost titulku dolu abyseto jakože nepřekrejvalo
    // protože byto bylo takový hnusný
    
    // x je dycky uprostřed
    property real stredX: width / 2
    property real stredY: height /2
    property real polomer: height / 2
    
    // polomer je vlastně jenom poloměr kružnice podle který namalujem nějak tlustej kroužek
    // noa půlka tloušťky tohodlenctoho kroužku je atribut polomerOffset
    property real polomerOffset: sirkaHlavniCary/2 + sirkaVedlejsiCary
        
    
    // naparsujem číslo podle toho jestli je float nebo celočíselný
    function prijmout(zprava)
    {
        hodnota = jeCelociselny ? parseInt(zprava) : parseFloat(zprava);
    }
    
    
    // výpočet poloměru podle toho kde se kružnice zdrcne s titulkem
    function aktualizovatPrumerTakyStred()
    {
        
        // když nemá titulek tak vemem to menčí z width/šířky a height/vejšky a podle toho uděláme ten poloměr
        // v polovině velikost - půlka tloušťky toho našeho malovacího kroužku
        if(!maTitulek)
        {
            if(!maLegendu)
            {
            kompas.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2;
            }
            else
            {
            kompas.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2 - Math.max(labelTop.paintedHeight, labelDown.paintedHeight, labelRight.paintedWidth, labelLeft.paintedWidth);
            
            kompas.stredY = kompas.height/2;
            return;

            }
        }
       // else if(titulek.width < stredX)
       else if(titulek.paintedWidth < stredX)
        {

            var c1 = (titulek.width - stredX)**2;
            var c2 = - titulek.height + kompas.height;
            var r = Math.abs(-(c1/(2*c2)) - (c2/2));
            
            if(!maLegendu)
            {
            kompas.polomer = Math.abs((Math.min(kresliciPlocha.width/2, kresliciPlocha.height/2, r) - polomerOffset));
            }
            else
            {
            kompas.polomer = Math.abs((Math.min(kresliciPlocha.width/2, kresliciPlocha.height/2, r) - polomerOffset) - Math.max(labelTop.paintedHeight, labelDown.paintedHeight, labelRight.paintedWidth, labelLeft.paintedWidth));
            kompas.stredY = kompas.height/2;
            return;
            }
            
        }
        else
        {
            if(!maLegendu)
            {
            kompas.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height - titulek.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2;
            }
            else
            {
            kompas.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height - titulek.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2 - Math.max(labelTop.paintedHeight, labelDown.paintedHeight, labelRight.paintedWidth, labelLeft.paintedWidth);
                        kompas.stredY = kompas.height/2;
            return;
            }
        }
        
        kompas.stredY = kompas.height/2;
        kompas.stredY = kompas.height - kompas.polomer - polomerOffset;
    }
    
    function uhelNaBod(uhel,polomer)
    {
        let x = Math.cos(uhel*Math.PI/180) * polomer;
        let y = Math.sin(uhel*Math.PI/180) * polomer;
        return {'x':x, 'y':y};
    }
    
    function hodnotaNaUhel()
    {
        return parseInt((hodnota/rosahHodnot) * 360);
    }
    
    // ždycky když se změní šířka/vejška widgetu tak přepočitáme ipsilonovou souřadnici středu kroužku + poloměr
    // fakt byse asi jako hodilo vědět jak se jako dělá callback na změnu rozlišení velikosti widgetu a tak si ty hodnoty přepočítávat :D
    onWidthChanged:
    {
        aktualizovatPrumerTakyStred();
    }
    onHeightChanged:
    {
        aktualizovatPrumerTakyStred();
    }

    // překreslíme malovací plochu pokaždý když se změní hodnota
    onHodnotaChanged:
    {
        kresliciPlocha.requestPaint();
        
        // tadleta aktualizace průměrů je tady jenom prozatim než se ňák podaří vymyslet jak to jako
        // aktualizovat nějak líp :O :O
        aktualizovatPrumerTakyStred();
    }
    // .. a taky když vyrobíme element
    Component.onCompleted:
    {
        hodnota = 0;
        kresliciPlocha.requestPaint();
    }

    //pozadi
    Rectangle
    {
        width: kompas.polomer * 2
        height: width
        color: kompas.barvaPozadi
        radius: width/2
        x: kompas.stredX - width/2
        y: kompas.stredY - height/2
        visible:true
    }
    
    //kreslicí plocha na který si budem malovat ty kroužky
    Canvas
    {
        id: kresliciPlocha
        width: kompas.width
        height: kompas.height
        
        // všecko je víc hežčí s antialiasingem :D ;D
        antialiasing: true

        onPaint:
        {
            // vezmem 2d kreslicí kontext a vypucujem 
            var ctx = getContext('2d');
            ctx.clearRect(0, 0, kresliciPlocha.width, kresliciPlocha.height);
            
            // namalování takový ty jakože střelky kompasový
            ctx.fillStyle = barvaAktivni;
            ctx.strokeStyle = barvaAktivni;
            ctx.beginPath();
            uhel = hodnotaNaUhel();
            let spicka = uhelNaBod(-90+uhel, kompas.polomer * 0.65 );
            let levej = uhelNaBod(135+uhel, kompas.polomer * 0.4 );
            let pravej = uhelNaBod(45+uhel, kompas.polomer * 0.4 );
            ctx.moveTo(spicka.x + kompas.stredX,spicka.y + kompas.stredY);
            ctx.lineTo(pravej.x + kompas.stredX,pravej.y + kompas.stredY);
            ctx.lineTo(kompas.stredX,kompas.stredY);
            ctx.lineTo(levej.x + kompas.stredX,levej.y + kompas.stredY);
            ctx.lineTo(spicka.x + kompas.stredX,spicka.y + kompas.stredY);

            
            ctx.fill();
            

            // a nakreslení vobou dvou krajních vokrajovejch čar
            ctx.beginPath();
            ctx.lineWidth = kompas.sirkaHlavniCary;
            ctx.strokeStyle = kompas.barvaTextu;
            ctx.arc(kompas.stredX, kompas.stredY, kompas.polomer , 0, 2*Math.PI);
            ctx.stroke();

            ctx.lineWidth = kompas.sirkaVedlejsiCary;
            ctx.beginPath();
            for(var i=0;i<kompas.pocetKroku;i++)
            {
                
                let bod = uhelNaBod(i*(360/(pocetKroku)) - 90, kompas.polomer);
                let bod2 = uhelNaBod(i*(360/(pocetKroku)) - 90, kompas.polomer*0.8);
                ctx.moveTo(bod2.x + kompas.stredX, bod2.y + kompas.stredY);
                ctx.lineTo(bod.x + kompas.stredX,bod.y + kompas.stredY);
            }
            ctx.stroke();
            
            ctx.lineWidth = kompas.sirkaHlavniCary;
            ctx.beginPath();
            for(var uhel=0;uhel<360;uhel+=90)
            {
                let bod = uhelNaBod(uhel, kompas.polomer);
                let bod2 = uhelNaBod(uhel, kompas.polomer*0.7);
                ctx.moveTo(bod2.x + kompas.stredX, bod2.y + kompas.stredY);
                ctx.lineTo(bod.x + kompas.stredX,bod.y + kompas.stredY);
            }

            ctx.stroke();

        }
        


    }
    
    Text
    {
        id: labelTop
        text: legendaTop
        color: parent.barvaTextu
        horizontalAlignment: Text.AlignHCenter

        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit
        
        y: kompas.stredY - kompas.polomer - kompas.offsetLegendy - paintedHeight
        
        width: parent.width
        height: parent.height/10
        visible: true
    }
    
    Text
    {
        id: labelRight
        text: legendaRight
        color: parent.barvaTextu
        anchors.verticalCenter: parent.verticalCenter 

        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit
        
        width: parent.width
        height: parent.height/10
        x: kompas.stredX + kompas.polomer + kompas.offsetLegendy
        visible: true
    }
    
    Text
    {
        id: labelDown
        text: legendaDown
        color: parent.barvaTextu
        horizontalAlignment: Text.AlignHCenter
        y: kompas.stredY + kompas.polomer + kompas.offsetLegendy
        
        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit
        
        width: parent.width
        height: parent.height/10
        visible: true
    }
    
        Text
    {
        id: labelLeft
        text: legendaLeft
        color: parent.barvaTextu
        anchors.verticalCenter: parent.verticalCenter 

        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit
        

        height: parent.height/10
        x: kompas.stredX - kompas.polomer - paintedWidth - kompas.offsetLegendy
        visible: true
    }


}


