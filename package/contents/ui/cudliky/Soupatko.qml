import QtQml 2.2
import QtQuick 2.15
import org.kde.plasma.components 3.0 as PlasmaComponents

// takový posouvátko
// jeto taková jakože vlastní trošičku horší varianta něčeho na
// způsob slideru https://doc.qt.io/qt-5/qml-qtquick-controls-slider.html
// máme nějakej čtvereček pomenovanej 'šoupací element' a ten mužem tahat po ose x

Cudlik
{
    id: soupatkoRoot
    nazev: 'soupatko'
    
    property int hodnotaMin: 0
    property int hodnotaMax: 100
    
    property string znakyZaHodnotou: '%'
    titulek.text: nazev + ' ' + 50 + znakyZaHodnotou
    
    
    function prijmout(zprava)
    {
        if(!muzePrijimat)
            return;
        
        var podil = (parseInt(zprava) - hodnotaMin) / (hodnotaMax - hodnotaMin);
        soupaciElement.x = maxRosah * podil + velikostOkraje;
        
        // jestli zobrazuje titulek za název připišem aktuální hodntou a znaky který za hodnotou ukazujem 
        if(maTitulek)
        {
            titulek.text = nazev + " " + poziceNaHodnotu() + znakyZaHodnotou;
        }
    }
    
    // přepočet xový hodnoty šoupacího elementu na hodntou
    function poziceNaHodnotu()
    {
        var podil = (soupaciElement.x - velikostOkraje) / maxRosah;
        return parseInt((hodnotaMax - hodnotaMin) * podil + hodnotaMin);
    }
    
    // možnej rosah xový souřadnice šoupacího elementu 
    property real maxRosah: okraj.width - soupaciElement.width - velikostOkraje*2
    
    // pozadí šoupátka
    Rectangle
    {
        id: pozadi
        height: parent.height/4
        width: parent.width

        anchors.verticalCenter: parent.verticalCenter
        color: barvaPozadi
        radius: zakulaceni
        
        // když pozadí máčken myšičkou tak taky aktualizujem polohu šoupátka + hodnotu čudliku
        MouseArea
        {
            anchors.fill: parent
            enabled: muzePublikovat
            onClicked:
            {
                var x = mouse.x;
                if(x < velikostOkraje)
                    x = velikostOkraje;
                else if(x > maxRosah)
                    x = maxRosah + velikostOkraje;
                
                soupaciElement.x = x;
                zpravaOdCudliku(mqttTopic, poziceNaHodnotu());
            }
        }
        
        // něco jako takovej jakože progress bar kterej vyplňuje voblast vod levýho vokraje až po šoupací element
        Rectangle
        {
            id: progress
            x: 1
            y: 1
            height: pozadi.height - 2
            width: soupaciElement.x + soupaciElement.width/2 -2
            color: barvaAktivni
            radius: zakulaceni
        }
        
        // vokraj šoupátka
        Rectangle
        {
            id: okraj
            border.color: barvaKontury
            border.width: velikostOkraje
            color: Qt.rgba(0,0,0,0);
            height: pozadi.height
            width: pozadi.width
            radius: zakulaceni
        }
        
        // samotnej šoupací element
        Rectangle
        {
            id: soupaciElement
            width: 32
            anchors.verticalCenter: pozadi.verticalCenter
            height: pozadi.height * 2
            radius: zakulaceni
            color: barvaTextu
            border.color: barvaKontury
            border.width: velikostOkraje
            
            // ždycky když se změní xová souřadnice tak aktualizujem titulek jestli ho zobrazujem teda
            onXChanged:
            {
                if(maTitulek)
                    titulek.text = nazev + ' ' + poziceNaHodnotu() + znakyZaHodnotou;
            }
            
            MouseArea
            {
                anchors.fill: parent
                enabled: muzePublikovat
                
                // uděláme ho dragable/tahatelnej myšičkou
                drag.target: soupaciElement
                
                // tahat pude jenom po ose x
                drag.axis: Drag.XAxis
                
                // vodkaď až kam ho jako pude tahat
                drag.minimumX: velikostOkraje
                drag.maximumX: okraj.width - soupaciElement.width - velikostOkraje
                
                onReleased:
                {
                    zpravaOdCudliku(mqttTopic, poziceNaHodnotu(), mqttQos, mqttRetain);
                }
                
            }
        }
        
        
    }
    
    
}
