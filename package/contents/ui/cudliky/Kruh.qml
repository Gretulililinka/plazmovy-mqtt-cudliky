import QtQml 2.2
import QtQuick 2.9

// takový jakože kolečko
// kolečko uměj namalovat i kcharts ale to sem zistila až když sem měla todleto hotový :D

Cudlik
{
    id: kruh
    
    nazev: "Kruh"
    
    // minimální maximální hodnota aktuální hodnota a možnej rosah hodnoty
    property real hodnotaMin: 0
    property real hodnotaMax: 100
    property real hodnota: 100
    property real rosahHodnot: hodnotaMax - hodnotaMin
    
    // jaký znaky se voběvujou za hodnotou uprostřed toho kruhu
    // výchozí je znak '%'
    property string znakyZaHodnotou: '%'
    titulek.text: nazev
    property real sirkaHlavniCary: 16
    property real sirkaVedlejsiCary: velikostOkraje
    property real offset_cary: 1
    
    // souřadnice středu toho kruhu a jeho poloměr
    // jeto vymyšlený tak že když je titulek kračí než zdálenost středu kruhu vod levýho/pravýho vokraje tak
    // se kruh do volnýho místa nafoukne. jinak je menčí než titulek a stčenej vo velikost titulku dolu abyseto jakože nepřekrejvalo
    // protože byto bylo takový hnusný
    
    // x je dycky uprostřed
    property real stredX: width / 2
    property real stredY: height /2
    property real polomer: height / 2
    
    // polomer je vlastně jenom poloměr kružnice podle který namalujem nějak tlustej kroužek
    // noa půlka tloušťky tohodlenctoho kroužku je atribut polomerOffset
    property real polomerOffset: sirkaHlavniCary/2 + sirkaVedlejsiCary
        
    // přepočet aktuální hodnoty na úhel v radiánech
    property real uhel: (kruh.hodnota - kruh.hodnotaMin) / kruh.rosahHodnot * 2 * Math.PI

    // posun úhlu aby nula byla na kružnici uplně nahoře (tam kde maj točicí hodiny dvanáctku)
    property real posunUhlu: -Math.PI / 2
    
    // naparsujem číslo podle toho jestli je float nebo celočíselný
    function prijmout(zprava)
    {
        hodnota = jeCelociselny ? parseInt(zprava) : parseFloat(zprava);
    }
    
    // výpočet poloměru podle toho kde se kružnice zdrcne s titulkem
    function aktualizovatPrumerTakyStred()
    {
        
        // když nemá titulek tak vemem to menčí z width/šířky a height/vejšky a podle toho uděláme ten poloměr
        // v polovině velikost - půlka tloušťky toho našeho malovacího kroužku
        if(!maTitulek)
        {
            kruh.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2 - offset_cary;
        }
        else if(titulek.width < stredX)
        {
            // když je titulek kratčí a dělá že končí před xovým středem nažeho zobrazovacího kroužku tak skusíme najít tu nejvíc nejvěčí kružnici
            // kterou tam mužem strčit. tadleta kružnice muže dosáhnout až na pravej dolní růžek titulku jestli ji to dovolej rozměry kreslicí plochy
            
            // do rovnice pro kružnici hele https://cs.wikipedia.org/wiki/Kru%C5%BEnice
            // (bod.x - střed.x)**2 + (bod.y - střed.y)**2 == r**2
            // si dosadíme za bod na vobvodu ten pravej dolní růžek titulku
            // a za střed.y vejšku kreslicí plochy - poloměr, jakože vejšku vod spodního vokraje
            // luštěnej zoreček třeba takle nějak
            // (titulek.x - kresliciPlocha.width/2)**2 + (titulek.y - kresliciPlocha.y + r)**2 == r**2
            
            // cejtim žeto de eště trošičku aritmetologicky víc zjednodušit ale sem moc líná nato :D
            // a tamta absolutní hodnota by tam asi jako vubec neměla bejt :D
            var c1 = (titulek.width - stredX)**2;
            var c2 = - titulek.height + kruh.height;
            var r = Math.abs(-(c1/(2*c2)) - (c2/2));
            
            kruh.polomer = Math.abs((Math.min(kresliciPlocha.width/2, kresliciPlocha.height/2, r) - polomerOffset) - offset_cary);
            
        }
        else
        {
            kruh.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height - titulek.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2 - offset_cary;
        }
        
        kruh.stredY = kruh.height - kruh.polomer - polomerOffset;
    }
    
    // ždycky když se změní šířka/vejška widgetu tak přepočitáme ipsilonovou souřadnici středu kroužku + poloměr
    // fakt byse asi jako hodilo vědět jak se jako dělá callback na změnu rozlišení velikosti widgetu a tak si ty hodnoty přepočítávat :D
    onWidthChanged:
    {
        aktualizovatPrumerTakyStred();
    }
    onHeightChanged:
    {
        aktualizovatPrumerTakyStred();
    }

    // překreslíme malovací plochu pokaždý když se změní hodnota
    onHodnotaChanged:
    {
        kresliciPlocha.requestPaint();
        
        // tadleta aktualizace průměrů je tady jenom prozatim než se ňák podaří vymyslet jak to jako
        // aktualizovat nějak líp :O :O
        aktualizovatPrumerTakyStred();
    }
    // .. a taky když vyrobíme element
    Component.onCompleted: kresliciPlocha.requestPaint()

    //kreslicí plocha na který si budem malovat ty kroužky
    Canvas
    {
        id: kresliciPlocha
        width: kruh.width
        height: kruh.height
        
        // všecko je víc hežčí s antialiasingem :D ;D
        antialiasing: true

        onPaint:
        {
            // vezmem 2d kreslicí kontext a vypucujem 
            var ctx = getContext('2d');
            //ctx.save();
            ctx.clearRect(0, 0, kresliciPlocha.width, kresliciPlocha.height);

            //nakreslení pozadí
            ctx.beginPath();
            ctx.lineWidth = sirkaHlavniCary;
            ctx.strokeStyle = kruh.barvaPozadi;
            ctx.arc(kruh.stredX, kruh.stredY, kruh.polomer, 0, 2*Math.PI);
            ctx.stroke();
            
            //nakreslení zobrazovací čáry
            ctx.beginPath();
            ctx.lineWidth = sirkaHlavniCary;
            ctx.strokeStyle = kruh.barvaAktivni;
            ctx.arc(kruh.stredX, kruh.stredY, kruh.polomer, kruh.posunUhlu, kruh.posunUhlu + kruh.uhel);
            ctx.stroke();

            // a nakreslení vobou dvou krajních vokrajovejch čar
            ctx.beginPath();
            ctx.lineWidth = sirkaVedlejsiCary;
            ctx.strokeStyle = kruh.barvaKontury
            ctx.arc(kruh.stredX, kruh.stredY, kruh.polomer - sirkaHlavniCary/2 - offset_cary, 0, 2*Math.PI);
            ctx.stroke();
            
            ctx.beginPath();
            ctx.lineWidth = sirkaVedlejsiCary;
            ctx.strokeStyle = kruh.barvaKontury
            ctx.arc(kruh.stredX, kruh.stredY, kruh.polomer + sirkaHlavniCary/2 + offset_cary, 0, 2*Math.PI);
            ctx.stroke();

            //ctx.restore();

        }

        // takovej ten text uprostřed kruhu
        Text
        {
            //anchors.horizontalCenter: parent.horizontalCenter
            y: kruh.stredY - paintedHeight/2
            x: kruh.stredX - paintedWidth/2
            //verticalAlignment: Text.AlignVCenter


            // jestli neni celočíselnej ukazujem proměnou jenom se dvouma desetinovejma místama
            text: ( jeCelociselny ? parseInt(kruh.hodnota) : kruh.hodnota.toFixed(2) ) + znakyZaHodnotou
            color: kruh.barvaTextu
            width: kruh.polomer*1.2
            height: kruh.polomer*1.2
            
            // velikost fontu se vybírá sama podle width/height velikosti Text voběktu
            font.pointSize: 256
            minimumPointSize: 8
            fontSizeMode: Text.Fit
        }
        
        // klikací voblast kruhu
        // je aktivní jenom když čudlik muže publikovat 
        // funguje tak že když se klikne někde na kruhu tak se hodnota nastaví tak aby jakože tomu 
        // kliknutýmu místo proporcionacionálně vodpovídala
        MouseArea
        {
            anchors.fill: parent
            enabled: muzePublikovat
            onClicked:
            {
                // spočitáme euklidovskou zdálenost
                var zdalenost = Math.sqrt( (mouse.x - kruh.stredX)**2 + (mouse.y - kruh.stredY)**2 );
                
                // nastavujem hodnotu jenom když se myškou máčkne přímo tamten kruh
                // věčí/menčí zdálenosti nepočitáme
                if(zdalenost > kruh.polomer - kruh.polomerOffset && zdalenost < kruh.polomer + kruh.polomerOffset)
                {      
                    // vodečtem vod máčknutý souřadnice střed kruhu takže střed kružnice je teďko jakoby na bode (0,0)
                    var x = mouse.x - kruh.stredX
                    var y = mouse.y - kruh.stredY
                    
                    // pomocí atan2 spočitáme jakej má bod máčknutí uhel v rosahu od mínus pí až plus pí
                    // tendleten uhel pak mužem převíst třeba na hodnotu v rosahu 0.0 až 2.0 atu pak násobit půlkou
                    // maximální možný hodnoty kterou čudlik muže ukazovat/mit 
                    kruh.hodnota = (1.0 - (Math.atan2(x, y) / Math.PI)) * kruh.rosahHodnot/2 + kruh.hodnotaMin;
                    
                    // nakonec pošlem
                    zpravaOdCudliku(mqttTopic, jeCelociselny ? parseInt(kruh.hodnota) : kruh.hodnota, mqttQos, mqttRetain);
                }
                
            }
        }
    }


}
