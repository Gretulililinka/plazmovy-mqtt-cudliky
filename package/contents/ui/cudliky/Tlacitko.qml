import QtQml 2.2
import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Templates 2.4 as QtSablony

Cudlik
{
    id: tlacitkoRoot
    
    nazev: 'Tlacitko'
    titulek.text: nazev
    
    property string napis: nazev
    property string coPosila: 'tlacitko macknuty'
    
    function prijmout(zprava)
    {
        if(!muzePrijimat)
            return;
        
        console.log('tlacitko by asi jako nemelo bejt vubec schopny prijimat mqtt zpravy :O :O');

    }

    Button
    {
        id: tlacitko
        
        width: parent.width
        height: maTitulek ? parent.height - titulek.height : parent.height
        y: maTitulek ? titulek.height : 0
        
        // nahradíme původní barvičky svejma nějakejma
        style: ButtonStyle 
        {
            
            background: Rectangle
            {
                height: control.height
                width: control.width
                color : tlacitko.pressed ? barvaAktivni : barvaPozadi
                border.color: barvaKontury
                border.width: velikostOkraje
                radius: zakulaceni
            }
            
            // text strčíme doprostřed a nafouknem aby byl co nejvíc nejvěčí
            label: Text    
            {

                verticalAlignment: Text.AlignVCenter
                anchors.centerIn: tlacitko
                text:  napis
                color: tlacitko.pressed ? barvaPozadi : barvaTextu
                width: control.width - velikostOkraje*2
                height: control.height - velikostOkraje*2
                
                font.pointSize: 256
                minimumPointSize: 8
                fontSizeMode: Text.Fit
            }
            

        }
        
        onClicked:
        {
            zpravaOdCudliku(mqttTopic, coPosila, mqttQos, mqttRetain);
        }
    }
    


}

