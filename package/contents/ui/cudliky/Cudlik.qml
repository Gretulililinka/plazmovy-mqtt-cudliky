import QtQml 2.2
import QtQuick 2.9
import QtQuick.Layouts 1.11

// oběkt ze kterýho 'děděj' všecky vostatní čudliky 

Rectangle
{

    //nějaká minimalní velikost aby se nám to jakože celý nescvrklo uplně
    Layout.minimumWidth: 64
    Layout.minimumHeight: 64
    
    property string nazev: 'Cudlik'
    
    // výchozí barvy čudliku kdyby třeba jako nebyly přenastavený po vyrobení
    property color barvaTextu: 'black'
    property color barvaAktivni: 'red'
    property color barvaKontury: 'green'
    property color barvaPozadi: 'white'
    
    // tloušťka vokraje a zakulacení vokrajů
    property real velikostOkraje: 5
    property real zakulaceni: 5
    
    // k jakýmu je připojenej topicu s jakým qos/kvalitou služby a jestli je zapnutej retain flag
    property string mqttTopic: ''
    property int mqttQos: 0
    property bool mqttRetain: false
    
    // jestli muže publikovat a přijímat zprávy
    property bool muzePublikovat: true
    property bool muzePrijimat: true
    
    // alias/reference kterou umožňujem potomkům lízt k titulku týdletý třídy
    property alias titulek: titulek
    // alias kterým se nastavuje viditelnost titulku
    property alias maTitulek: titulek.visible
    
    // jestli jakoby parsuje json a jestli jo tak jakej element hledá
    // žádný složitý parsování prostě koukne na element
    // potřebuje někdo nějaký víc složitější parsování??????? :O :O :O :O
    // sou různý mrňavý knihovničky nato hele třeba https://www.w3resource.com/JSON/JSONPath-with-JavaScript.php
    property bool parsujeJson: false
    property string jsonElement: ""
    
    // jestli je double/int
    property bool jeCelociselny: true
    
    // jestli je vubec jako číselnej
    property bool jeCiselny: true
    
    color: Qt.rgba(0,0,0,0)
    
    // funkce do který se budou strkat přijatý zprávy
    // nevim jak to udělat jako abstraktní metodu :D
    function prijmout(zprava){console.log("zavolano cudlik.prijmout rodicovsky tridy :O :O");}
    
    // funkce co zkusí vyparsovat klíč z json zprávičky aten pak strčit do normální funkce přijmout
    // potomci Čudliku todleto teda pak nemusej řešit
    function prijmoutJson(zpravaJson)
    {
        if(parsujeJson)
        {
            if(zpravaJson.hasOwnProperty(jsonElement))
            {
                prijmout(zpravaJson[jsonElement]);
            }
            else
            {
                console.error('v json zprave neni tendlecten klic/element!!!!!!!!!!!!!!!');
            }
        }
        else
        {
            console.error('tendle cudlik nechce json parsovat!!!!!!!!!!!!!!');
        }
        
    }
    
    // titulek čudliku kterej se věčinou zobrazuje nad tim elementem
    Text
    {
        id: titulek
        text: "Cudlik"
        color: parent.barvaTextu
//         font.pointSize: 16
            // velikost fontu se vybírá sama podle width/height velikosti Text voběktu
            font.pointSize: 256
            minimumPointSize: 8
            fontSizeMode: Text.Fit
        
        width: parent.width
        height: parent.height/5
        visible: true
    }
}
