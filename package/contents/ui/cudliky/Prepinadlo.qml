import QtQml 2.2
import QtQuick 2.15

// mačkátko který funguje jako takovej jakože přepínač
// má dva stavy zapnuto vypnuto

Cudlik
{
    id: prepinadlo
    
    nazev: 'prepinadlo'
    
    // jestli je zapnutej nebo vypnutej
    property bool stav: false
    
    // podle jakýho vobsahu chycený zprávy mění svuj stav
    // todleto taky posílá při zapnutí/vypnutí
    property string zpravaPriOn: '1'
    property string zpravaPriOff: '0'
    
    // co je na něm jako napsaný při zapnutí/vypnutí
    property string onText: 'ON'
    property string offText: 'OFF'
    
    // vypnem normální titulek třídy Čudlik
    // budem si malovat nějakej nezávislej na to mačkátko přímo
    titulek.visible: false

    // mačkátko je takovej jakože čtvereček
    // velikost strany čtverečku bude to víc menčí z dýlky a šířky elementu 
    property real stranaTlacitka: Math.min(width, height)
    
    function prijmout(zprava)
    {
        if(!muzePrijimat)
            return;
        
        // nastavíme stav podle toho jestli jako string zprávičky vodpovídá
        // zpravaPriOff nebo zpravaPriOn a aktualizujem zobrazení čudliku
        if(zprava === zpravaPriOff)
        {
            stav = false;
            aktualizovatCudlik();
        }
        else if(zprava === zpravaPriOn)
        {
            stav = true;
            aktualizovatCudlik();
        }
    }
    
    // prostě jako přebarvíme součástky čudliku podle stavu kterej má
    // a změníme vobsah zobrazovanejch textů
    function aktualizovatCudlik()
    {
        if(stav)
        {
            mackatko.color = barvaAktivni;
            onOffText.color = barvaKontury;
            onOffTitulek.color = barvaKontury;
            onOffText.text = onText;
        }
        else
        {
            mackatko.color = barvaPozadi;
            onOffText.color = barvaTextu;
            onOffTitulek.color = barvaTextu;
            onOffText.text = offText;
        }
        
    }
    
    // když se nám změní hodnota proměný stav tak taky aktualizujem zobrazení čudliku
    onStavChanged: aktualizovatCudlik()
    
    Rectangle
    {
        id: mackatko
        radius: zakulaceni
        
        width: parent.stranaTlacitka
        height: parent.stranaTlacitka
        
        // vypolohujem doprostředka
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        
        // barva a tloušťka vokraje rectanglu/čtverečku
        border.color: barvaKontury
        border.width: velikostOkraje
        
        MouseArea
        {
            id: mouseArea
            anchors.fill: parent
            enabled: muzePublikovat
            
            onClicked:
            {
                prepinadlo.stav = !prepinadlo.stav;
                zpravaOdCudliku(mqttTopic, prepinadlo.stav ? zpravaPriOn : zpravaPriOff, mqttQos, mqttRetain);
            }
        }
        
        // text uprostřed čudliku kterej popisuje jestli je mačkátko zapnutý/vypnutý
        Text
        {
            id: onOffText
            anchors.centerIn: mackatko
            
            verticalAlignment: Text.AlignVCenter
            width: parent.width/2
            height: parent.height/2
            
            font.pointSize: 256
            minimumPointSize: 8
            fontSizeMode: Text.Fit
        }
        
        // titulek/název čudliku zobrazovanej přímo na mačkátku
        Text
        {
            id: onOffTitulek
            text: prepinadlo.nazev
            
            anchors.horizontalCenter: parent.horizontalCenter
            
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            width: parent.width/2
            height: parent.height/2
            
            font.pointSize: 256
            minimumPointSize: 8
            fontSizeMode: Text.Fit
        }
    }

}
