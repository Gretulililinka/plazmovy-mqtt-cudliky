import QtQml 2.2
import QtQuick 2.9

Cudlik
{
    id: polarniGraf
    
    nazev: "PolarniGraf"
    
    // minimální maximální hodnota aktuální hodnota a možnej rosah hodnoty
    property real uhlovaHodnotaMin: 0
    property real uhlovaHodnotaMax: 360
    property real rosahHodnot: uhlovaHodnotaMax - uhlovaHodnotaMin
    
    property real pocetKroku: 21
    property real rosahKroku: rosahHodnot / pocetKroku
        
    // maximalni neuhlova hodnota 
    property real maxHodnota: 0
    
    property variant hodnoty: []
    property variant sumy: []
    property variant pocty: []
    
    property variant fronta: []
    property real maxDylkaFronty: 64
    
    property string klicHodnot: 'bla'
    property string klicUhloveHodnoty: 'bla'
    
    property bool maLegendu: true
    property string legendaTop: 'top'
    property string legendaDown: 'down'
    property string legendaLeft: 'left'
    property string legendaRight: 'right'
    property real offsetLegendy: 5
    
    // jaký znaky se voběvujou za hodnotou uprostřed toho polarniGrafu
    // výchozí je znak '%'
    titulek.text: nazev
    property real sirkaHlavniCary: 4
    property real sirkaVedlejsiCary: 2
    
    property color barvaVyplneni: Qt.rgba(barvaAktivni.r, barvaAktivni.g, barvaAktivni.b, barvaAktivni.a * 0.5)

    // souřadnice středu toho polarniGrafu a jeho poloměr
    // jeto vymyšlený tak že když je titulek kračí než zdálenost středu polarniGrafu vod levýho/pravýho vokraje tak
    // se polarniGraf do volnýho místa nafoukne. jinak je menčí než titulek a stčenej vo velikost titulku dolu abyseto jakože nepřekrejvalo
    // protože byto bylo takový hnusný
    
    // x je dycky uprostřed
    property real stredX: width / 2
    property real stredY: height /2
    property real polomer: height / 2
    
    // polomer je vlastně jenom poloměr kružnice podle který namalujem nějak tlustej kroužek
    // noa půlka tloušťky tohodlenctoho kroužku je atribut polomerOffset
    property real polomerOffset: sirkaHlavniCary/2 + sirkaVedlejsiCary
        
    // přepočet aktuální hodnoty na úhel v radiánech
    //property real uhel: (polarniGraf.hodnota - polarniGraf.hodnotaMin) / polarniGraf.rosahHodnot * 2 * Math.PI

    // posun úhlu aby nula byla na kružnici uplně nahoře (tam kde maj točicí hodiny dvanáctku)
    //property real posunUhlu: -Math.PI / 2
    
    // bere jenom json
    function prijmoutJson(zpravaJson)
    {
        if(parsujeJson)
        {
            if(zpravaJson.hasOwnProperty(klicHodnot) && zpravaJson.hasOwnProperty(klicUhloveHodnoty))
            {
                pridat(zpravaJson[klicHodnot], zpravaJson[klicUhloveHodnoty]);
                aktualizovatPrumerTakyStred();
                kresliciPlocha.requestPaint();
                
            }
            else
            {
                console.error('v json zprave pro polarni graf neni klic/element!!!!!!!!!!!!!!!');
            }
        }
        else
        {
            console.error('polarni graf musi json parsovat!!!!!!!!!!!!!!');
        }
        
    }
    
    function pridat(hodnota, uhlovaHodnota)
    {
        let index = uhlovaHodnotaNaIndex(uhlovaHodnota);
        
        sumy[index]+=hodnota;
        pocty[index]+=1;
        var index_rmvd = index;

        fronta.push([hodnota, index]);
        var rmvd = fronta.shift();

        if(rmvd != undefined)
        {

            var hodnota_rmvd = rmvd[0];
            index_rmvd = rmvd[1];
            sumy[index_rmvd] -= hodnota_rmvd;
            pocty[index_rmvd] -= 1;
        }


        if(pocty[index]!=0)
        {
            hodnoty[index] = sumy[index]/pocty[index];
        }
        else
        {
            sumy[index] = 0.0;
            hodnoty[index] = 0.0;
        }
        if(index_rmvd != index)
        {
            if(pocty[index_rmvd]!=0)
            {
                hodnoty[index_rmvd] = sumy[index_rmvd]/pocty[index_rmvd];
            }
            else
            {
                sumy[index_rmvd] = 0.0;
                hodnoty[index_rmvd] = 0.0;
            }
        }
        
        maxHodnota = Math.max(...hodnoty);
        
    }
    
    // výpočet poloměru podle toho kde se kružnice zdrcne s titulkem
    function aktualizovatPrumerTakyStred()
    {
        
        // když nemá titulek tak vemem to menčí z width/šířky a height/vejšky a podle toho uděláme ten poloměr
        // v polovině velikost - půlka tloušťky toho našeho malovacího kroužku
        if(!maTitulek)
        {
            if(!maLegendu)
            {
            polarniGraf.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2;
            }
            else
            {
            polarniGraf.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2 - Math.max(labelTop.paintedHeight, labelDown.paintedHeight, labelRight.paintedWidth, labelLeft.paintedWidth);
            
            polarniGraf.stredY = polarniGraf.height/2;
            return;

            }
        }
       // else if(titulek.width < stredX)
       else if(titulek.paintedWidth < stredX)
        {

            var c1 = (titulek.width - stredX)**2;
            var c2 = - titulek.height + polarniGraf.height;
            var r = Math.abs(-(c1/(2*c2)) - (c2/2));
            
            if(!maLegendu)
            {
            polarniGraf.polomer = Math.abs((Math.min(kresliciPlocha.width/2, kresliciPlocha.height/2, r) - polomerOffset));
            }
            else
            {
            polarniGraf.polomer = Math.abs((Math.min(kresliciPlocha.width/2, kresliciPlocha.height/2, r) - polomerOffset) - Math.max(labelTop.paintedHeight, labelDown.paintedHeight, labelRight.paintedWidth, labelLeft.paintedWidth));
            polarniGraf.stredY = polarniGraf.height/2;
            return;
            }
            
        }
        else
        {
            if(!maLegendu)
            {
            polarniGraf.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height - titulek.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2;
            }
            else
            {
            polarniGraf.polomer = (Math.min(kresliciPlocha.width, kresliciPlocha.height - titulek.height) - sirkaHlavniCary - sirkaVedlejsiCary) / 2 - Math.max(labelTop.paintedHeight, labelDown.paintedHeight, labelRight.paintedWidth, labelLeft.paintedWidth);
                        polarniGraf.stredY = polarniGraf.height/2;
            return;
            }
        }
        
        polarniGraf.stredY = polarniGraf.height/2;
        polarniGraf.stredY = polarniGraf.height - polarniGraf.polomer - polomerOffset;
    }
    
    function uhelNaBod(uhel,polomer)
    {
        let x = Math.cos(uhel*Math.PI/180) * polomer;
        let y = Math.sin(uhel*Math.PI/180) * polomer;
        return {'x':x, 'y':y};
    }
    
    function uhlovaHodnotaNaIndex(hodnota)
    {
        hodnota = hodnota % 360;
        let index = Math.floor(hodnota / rosahKroku);
        
        // prvni a posledni sou voba ten samej (tam co maj točicí hodiny 12)
        if(index == pocetKroku-1)
            index=0;

        return index;
    }
    
    // ždycky když se změní šířka/vejška widgetu tak přepočitáme ipsilonovou souřadnici středu kroužku + poloměr
    // fakt byse asi jako hodilo vědět jak se jako dělá callback na změnu rozlišení velikosti widgetu a tak si ty hodnoty přepočítávat :D
    onWidthChanged:
    {
        aktualizovatPrumerTakyStred();
    }
    onHeightChanged:
    {
        aktualizovatPrumerTakyStred();
    }

    // překreslíme malovací plochu pokaždý když se změní hodnota
    onFrontaChanged:
    {
        kresliciPlocha.requestPaint();
        
        // tadleta aktualizace průměrů je tady jenom prozatim než se ňák podaří vymyslet jak to jako
        // aktualizovat nějak líp :O :O
        aktualizovatPrumerTakyStred();
    }
    // .. a taky když vyrobíme element
    Component.onCompleted:
    {
        hodnoty = Array(pocetKroku-1).fill(0);
        sumy = Array(pocetKroku-1).fill(0);
        pocty = Array(pocetKroku-1).fill(0);
        fronta = Array(maxDylkaFronty).fill(undefined);
        
        kresliciPlocha.requestPaint();

    }

    Rectangle
        {
            width: polarniGraf.polomer * 2
            height: width
            color: polarniGraf.barvaPozadi
            radius: width/2
            x: polarniGraf.stredX - width/2
            y: polarniGraf.stredY - height/2
            visible:true
        }
    
    //kreslicí plocha na který si budem malovat ty kroužky
    Canvas
    {
        id: kresliciPlocha
        width: polarniGraf.width
        height: polarniGraf.height
        
        // všecko je víc hežčí s antialiasingem :D ;D
        antialiasing: true

        onPaint:
        {
            // vezmem 2d kreslicí kontext a vypucujem 
            var ctx = getContext('2d');
            //ctx.save();
            ctx.clearRect(0, 0, kresliciPlocha.width, kresliciPlocha.height);
            
            //nakreslení zobrazovací čáry
            ctx.beginPath();
            ctx.lineWidth = sirkaHlavniCary;
            ctx.strokeStyle = polarniGraf.barvaAktivni;
            ctx.arc(polarniGraf.stredX, polarniGraf.stredY, polarniGraf.polomer, polarniGraf.posunUhlu, polarniGraf.posunUhlu + polarniGraf.uhel);
            ctx.stroke();
            ctx.lineWidth = sirkaVedlejsiCary;
            
            
            // nakreslení polygonu hodnot
            ctx.fillStyle = barvaVyplneni;
            ctx.strokeStyle = barvaAktivni;
            ctx.beginPath();
            
            let bod = uhelNaBod(-90, (polarniGraf.polomer - sirkaHlavniCary) * (hodnoty[0]/maxHodnota) );
            ctx.moveTo(bod.x + polarniGraf.stredX,bod.y + polarniGraf.stredY);
            for(var i=1;i<polarniGraf.pocetKroku-1;i++)
            {
                let bod = uhelNaBod(i*(360/(pocetKroku-1))-90, (polarniGraf.polomer - sirkaHlavniCary) * (hodnoty[i]/maxHodnota));
                ctx.lineTo(bod.x + polarniGraf.stredX,bod.y + polarniGraf.stredY);
            }
            
            ctx.fill();
            
            
            // okraj
            ctx.beginPath();
            
            bod = uhelNaBod(-90, (polarniGraf.polomer - sirkaHlavniCary) * (hodnoty[0]/maxHodnota) );
            ctx.moveTo(bod.x + polarniGraf.stredX,bod.y + polarniGraf.stredY);
            
            //fix divnyho vykreslovani mimo voblast grafu kdyz hodnota ne ma zadny sousedy
            // (vipada to jako chyba ve vypoctu prumeru ale tim to neni :O :O asi chyba v qml )
            if(hodnoty[0]/maxHodnota > 0.9999 && hodnoty[pocetKroku-2]/maxHodnota < 0.0001 && hodnoty[1] < 0.0001)
            {
                ctx.lineTo(bod.x + polarniGraf.stredX + 1,bod.y + 1 + polarniGraf.stredY);
                ctx.lineTo(polarniGraf.stredX + 1,1 + polarniGraf.stredY);
            }
            
            for(var i=1;i<polarniGraf.pocetKroku-1;i++)
            {
                
                let bod = uhelNaBod(i*(360/(pocetKroku-1))-90, (polarniGraf.polomer - sirkaHlavniCary) * (hodnoty[i]/maxHodnota));
                ctx.lineTo(bod.x + polarniGraf.stredX,bod.y + polarniGraf.stredY);
                
                // taky fix jako u prvniho bodu
                let nextIndex = (i < pocetKroku -2 ) ? i+1 : 0;
                if( hodnoty[i]/maxHodnota > 0.9999 && hodnoty[i-1]/maxHodnota < 0.0001 && hodnoty[nextIndex] < 0.0001)
                {
                    ctx.lineTo(bod.x + polarniGraf.stredX + 1,bod.y + 1 + polarniGraf.stredY);
                    ctx.lineTo(polarniGraf.stredX + 1,1 + polarniGraf.stredY);
                }

            }
            
            ctx.lineTo(bod.x + polarniGraf.stredX,bod.y + polarniGraf.stredY);
            ctx.stroke();
            
            

            // a nakreslení vobou dvou krajních vokrajovejch čar
            ctx.beginPath();
            ctx.lineWidth = polarniGraf.sirkaHlavniCary;
            ctx.strokeStyle = polarniGraf.barvaTextu;
            ctx.arc(polarniGraf.stredX, polarniGraf.stredY, polarniGraf.polomer , 0, 2*Math.PI);
            ctx.stroke();
            ctx.lineWidth = sirkaVedlejsiCary;
            ctx.beginPath();
            ctx.arc(polarniGraf.stredX, polarniGraf.stredY, polarniGraf.polomer*0.75 , 0, 2*Math.PI);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(polarniGraf.stredX, polarniGraf.stredY, polarniGraf.polomer/2 , 0, 2*Math.PI);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(polarniGraf.stredX, polarniGraf.stredY, polarniGraf.polomer/4 , 0, 2*Math.PI);
            //ctx.arc(polarniGraf.stredX, polarniGraf.stredY, polarniGraf.polomer - sirkaHlavniCary/2 - offset_cary, 0, 2*Math.PI);
            ctx.stroke();
            
            ctx.beginPath();
            for(var i=0;i<polarniGraf.pocetKroku-1;i++)
            {
                let bod = uhelNaBod(i*(360/(pocetKroku-1)) - 90, polarniGraf.polomer);
                let bod2 = uhelNaBod(i*(360/(pocetKroku-1)) - 90, polarniGraf.polomer/4);
                ctx.moveTo(bod2.x + polarniGraf.stredX, bod2.y + polarniGraf.stredY);
                ctx.lineTo(bod.x + polarniGraf.stredX,bod.y + polarniGraf.stredY);
            }
            ctx.stroke();
            
            ctx.beginPath();
            ctx.moveTo(polarniGraf.stredX, polarniGraf.stredY - polarniGraf.polomer/4);
            ctx.lineTo(polarniGraf.stredX,polarniGraf.stredY + polarniGraf.polomer/4);
            ctx.stroke();
            
            ctx.beginPath();
            ctx.moveTo(polarniGraf.stredX - polarniGraf.polomer/4, polarniGraf.stredY);
            ctx.lineTo(polarniGraf.stredX + polarniGraf.polomer/4,polarniGraf.stredY);
            ctx.stroke();


            //ctx.restore();

        }
        


    }
    
    Text
    {
        id: labelTop
        text: legendaTop
        color: parent.barvaTextu
        horizontalAlignment: Text.AlignHCenter

        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit
        
        y: polarniGraf.stredY - polarniGraf.polomer - polarniGraf.offsetLegendy - paintedHeight
        
        width: parent.width
        height: parent.height/10
        visible: true
    }
    
    Text
    {
        id: labelRight
        text: legendaRight
        color: parent.barvaTextu
        anchors.verticalCenter: parent.verticalCenter 
        //horizontalAlignment: Text.AlignRight
        
        //anchors.left: parent.left
        //verticalAlignment: Text.AlignVCenter
        //horizontalAlignment: Text.AlignRight
        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit
        
        width: parent.width
        height: parent.height/10
        x: polarniGraf.stredX + polarniGraf.polomer + polarniGraf.offsetLegendy
        visible: true
    }
    
    Text
    {
        id: labelDown
        text: legendaDown
        color: parent.barvaTextu
        //verticalAlignment: polarniGraf.AlignBottom
        //horizontalAlignment: polarniGraf.AlignHCenter
        //anchors.up: polarniGraf.down
        horizontalAlignment: Text.AlignHCenter
        //anchors.horizontalCenter: parent.horizontalCenter 
        //anchors.bottom: parent.bottom
        //anchors.fill: parent
        y: polarniGraf.stredY + polarniGraf.polomer + polarniGraf.offsetLegendy
        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit
        
        width: parent.width
        height: parent.height/10
        visible: true
    }
    
        Text
    {
        id: labelLeft
        text: legendaLeft
        color: parent.barvaTextu
        anchors.verticalCenter: parent.verticalCenter 
        //anchors.left: parent.left
        
        //verticalAlignment: Text.AlignVCenter
        //horizontalAlignment: Text.AlignRight
        font.pointSize: 256
        minimumPointSize: 8
        fontSizeMode: Text.Fit
        

        height: parent.height/10
        x: polarniGraf.stredX - polarniGraf.polomer - paintedWidth - polarniGraf.offsetLegendy
        visible: true
    }


}

