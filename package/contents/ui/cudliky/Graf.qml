import QtQuick 2.9
import QtQuick.Controls 2.5
import org.kde.quickcharts 1.0 as Charts
import org.kde.quickcharts.controls 1.0 as ChartsControls

// čudlik s kcharts grafem
// api maj trošičku popsaný tady https://api.kde.org/frameworks-api/frameworks-apidocs/frameworks/kquickcharts/html/index.html
// ale blbě se k tomu hledaj nějaký víc složitější ukázkový příklady :O :/

Cudlik 
{
    id: grafRoot
    nazev: "graf"
    titulek.text: nazev
    
    property real zobrazovanaPromena: 0
    property var zdrojDat: Charts.SingleValueSource { value: zobrazovanaPromena }
    property int pocetCarMrizky: 3
    
    function prijmout(zprava)
    {
        grafRoot.zobrazovanaPromena = jeCelociselny ? parseInt(zprava) : parseFloat(zprava);
        grafRoot.zdrojDat.dataChanged();
    }
        
    Rectangle
    {
        width: parent.width
        height: titulek.visible ? parent.height - titulek.height : parent.height
        y: titulek.visible ? titulek.height : 0
        
        color: barvaPozadi
        radius: zakulaceni
        
        
        Charts.LineChart
        {
            id: grafPlot
            antialiasing: true
            anchors.verticalCenter: parent.verticalCenter
            x: yAxisLabels.width + velikostOkraje + 10

            width: parent.width - velikostOkraje*2 - yAxisLabels.width -10
            height: parent.height - velikostOkraje*2
        
            colorSource: Charts.SingleValueSource { value: barvaAktivni }
            nameSource: Charts.SingleValueSource { value: nazev }
            valueSources: Charts.HistoryProxySource 
            {
                source: grafRoot.zdrojDat
                maximumHistory: 100
            }
                
            lineWidth: 2
            fillOpacity: 0.5
            smooth:false
            direction:Charts.XYChart.ZeroAtEnd
                
                
        
            Charts.GridLines
            {
                id: vertikalni
    
                anchors.fill: parent
                chart: parent
                direction: Charts.GridLines.Vertical;
    
                minor.count: pocetCarMrizky
                minor.lineWidth: 1
                minor.color: barvaTextu
                
                // major čáry se mi nepodařilo uplně vypnout
                // když se třeba jako do major.count strčí 0 tak si je to asi generuje podle .frequency atributu :O :/
                major.count: 1
                major.lineWidth: 1
                major.color: barvaTextu
           
            }
            
            Charts.GridLines
            {
                id: horizontalni
    
                anchors.fill: parent
                chart: parent
                direction: Charts.GridLines.Horizontal;
    
                minor.count: pocetCarMrizky
                minor.lineWidth: 1
                minor.color: barvaTextu
                
                major.count: 1
                major.lineWidth: 1
                major.color: barvaTextu
            }
            
            Rectangle
            {
                id: okrajMrizky
                anchors.fill: parent
                color: Qt.rgba(0,0,0,0)
                border.color: barvaTextu
                border.width: 1
            }
            
        }
            
        Charts.AxisLabels
        {
            id: yAxisLabels
            anchors.verticalCenter: parent.verticalCenter

            height: parent.height - velikostOkraje*2
            x: 10
    
            direction: Charts.AxisLabels.VerticalBottomTop
            
            // nejde to ňák víc líp???? :O :O
            delegate: Label
            {
                color: barvaTextu
                text: parseFloat(Charts.AxisLabels.label).toFixed(2);
            }
            
            source: Charts.ChartAxisSource 
            {
                chart: grafPlot
                axis: Charts.ChartAxisSource.YAxis
                itemCount: pocetCarMrizky + 2
            }
        }
    
    
        Rectangle
        {
            id: okrajGrafu
            anchors.fill: parent
            color: Qt.rgba(0,0,0,0)
            border.color: barvaKontury
            border.width: velikostOkraje
            radius: zakulaceni
        }
    
    }
}

