import QtQml 2.2
import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Templates 2.4 as QtSablony

//jeto vlastně combobox ve kterým je strčený nějaký políčko stringů ze kterýho si mužem vybírat jednu možnost dycky

Cudlik
{
    id: moznostiRoot
    
    nazev: 'moznosti'
    titulek.text: nazev
    property var seznamMoznosti: ['tady','by měl', 'bejt seznam','možností']
    
    function prijmout(zprava)
    {
        if(!muzePrijimat)
            return;

        // jestli přijatá zpráva vodpovídá některý hodnotě v poli možností tak se nastaví 
        // index právě vybraný možnosti
        // ( atribut currentText qml voběktu https://doc.qt.io/qt-5/qml-qtquick-controls-combobox-members.html je jenom pro čtení :O :/ )
        if(seznamMoznosti.includes(zprava))
                vybiradlo.currentIndex = vybiradlo.find(zprava);
    }

    ComboBox
    {
        id: vybiradlo
        
        width: parent.width
        height: maTitulek ? parent.height - titulek.height : parent.height
        y: maTitulek ? titulek.height : 0
        
        model: seznamMoznosti
        
        // možnost je vybíratelná jenom když de publikovat
        selectByMouse: muzePublikovat
        
        // nahradíme původní barvičky tlačítka svejma nějakejma
        // neví někdo jak jako přestylovat do dropdownový menu :O :O
        // tendlecten návod hele https://doc.qt.io/qt-5/qtquickcontrols2-customize.html#customizing-combobox nefunguje :O :/
        style: ComboBoxStyle 
        {
            
            font.pointSize: 24
            textColor: 'black'
            selectedTextColor: barvaKontury
            selectionColor: barvaAktivni
            
            background: Rectangle
            {
                height: control.height
                width: control.width
                color : barvaPozadi
                border.color: barvaKontury
                border.width: velikostOkraje
                radius: zakulaceni
            }
            
            // text strčíme doprostřed a nafouknem aby byl co nejvíc nejvěčí
            label: Text    
            {

                //verticalAlignment: Text.AlignVCenter
                anchors.centerIn: vybiradlo
                text:  control.editText
                color: barvaTextu
                width: control.width - velikostOkraje*2
                height: control.height - velikostOkraje*2
                
                font.pointSize: 256
                minimumPointSize: 8
                fontSizeMode: Text.Fit
            }
            
        }
        
        onActivated:
        {
            zpravaOdCudliku(mqttTopic, seznamMoznosti[index], mqttQos, mqttRetain);
        }
    }
    


}
