import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.4 as Kirigami

// podle ukázky tady hele https://develop.kde.org/docs/plasma/widget/configuration/

// naimportujem si svuj plugin kuli třídě 'NačítadloSouboru' kterou potřebujem kuli načtení textovýho souboru
// samotný qml nic takovýho jako přečíst nějakej *.txt asi neumí :O :O
import org.kde.private.mqttCudliky 1.0

Kirigami.FormLayout
{
    id: page
    Kirigami.FormData.label: i18n('Nastavování')

    // vyrobení načítadla
    Nacitadlo
    {
        id: nacitadlo
    }
    
    // proměný musej mit ten prefix 'cfg_' aby si to jakože spárovalo s vodpovídajícíma hodnotama v souboru config.xml 
    property alias cfg_adresa: konfiguraceAdresa.text
    property alias cfg_id: konfiguraceId.text
    property alias cfg_uzivatel: konfiguraceUzivatel.text
    property alias cfg_heslo: konfiguraceHeslo.text
  
    property alias cfg_konfiguracniJson: configTxt.text
    
    TextField
    {
        id: konfiguraceAdresa
        Kirigami.FormData.label: i18n('adresa:')
        placeholderText: i18n('adresa s druhem protokolu a portem nakonci. třeba něco jako tcp://127.0.0.1:1883')
    }
        
    TextField
    {
        id: konfiguraceId
        Kirigami.FormData.label: i18n('id klienta')
        placeholderText: i18n('id klienta')
    }

    TextField
    {
        id: konfiguraceUzivatel
        Kirigami.FormData.label: i18n('uživatel')
        placeholderText: i18n('uživatel')
    }
        
    TextField
    {
        id: konfiguraceHeslo
        Kirigami.FormData.label: i18n('heslo')
        placeholderText: i18n('heslo')
    }

    
    ColumnLayout
    {
    Kirigami.FormData.label: i18n('načíst z konfiguračního *.json (umí to načíst i tamto nastavování brokera takže seto nemusí vyplňovat rukama)')
    
    
        TextArea
        {
        id: configTxt
        placeholderText: i18n('tady by měl bejt konfurační json čudliků ukázanej')
        Layout.fillHeight: true
        Layout.fillWidth: true
        }

        Button
        {
            text: i18n('Načíst *.json ze souboru')
            icon.name: 'folder-symbolic'
            onClicked:
            {
                fileDialogLoader.active = true
            }

            // načítadlo souborů
            // soubor dostanem jako proměnou fileUrl toje místo uložení toho souboru někde v systemu
            // asi nám neumí dát přímo jeho vobsah nevim :O :O
            Loader
            {
                id: fileDialogLoader
                active: false

                sourceComponent: FileDialog 
                {
                    id: fileDialog
                    //folder: shortcuts.music
                    
                    // chcem json ale umožníme taky vybrat všecky soubory
                    nameFilters: [i18n('json(%1)', '*.json'),i18n('všecky soubory(%1)', '*'),]
                    
                    onAccepted:
                    {
                        // načtem načítadlem vobsah souboru do js proměný
                        var obsah = nacitadlo.nacistSoubor(fileUrl);
                        
                        // měl byto bejt jakože validní json nóó tak si ho zkusíme tady nejdřiv naparsovat a
                        // když nám to nebude/skončí vyjímkou tak tam asi jako je nějaká chyba
                        var json = null;
                        try
                        {
                            json = JSON.parse(obsah);
                        }
                        catch(e)
                        {
                            configTxt.text = 'chyba pri parsovani konfiguracniho *.json: ' + e.name + ', ' + e.message;
                            console.error(configTxt.text);
                        }
                        
                        if(json != null)
                        {
                            // jestli máme validní json tak ho strčíme do configTxt textovýho pole aby se nám jakože pak uložil do nastavení widgetu
                            configTxt.text = obsah;
                            
                            // jestli má json soubor element pomenovanej 'broker' tak podle něj nastavíme připojení a nemusíme to vyplňovat ručně furt
                            if(json.hasOwnProperty('broker'))
                            {
                                var brokerJson = json.broker;
                                
                                if(brokerJson.hasOwnProperty('adresa'))
                                    konfiguraceAdresa.text = brokerJson.adresa;
                                if(brokerJson.hasOwnProperty('id'))
                                    konfiguraceId.text = brokerJson.id;
                                if(brokerJson.hasOwnProperty('uzivatel'))
                                    konfiguraceUzivatel.text = brokerJson.uzivatel;
                                if(brokerJson.hasOwnProperty('heslo'))
                                    konfiguraceHeslo.text = brokerJson.heslo;
                            }
                            
                        }
                        
                        fileDialogLoader.active = false;
                    }
                    
                    onRejected:
                    {
                        fileDialogLoader.active = false;
                    }
                    
                    Component.onCompleted: open()
                }
            }
        }

    }
    
}
 
