#include "KlientProMQTT.h"

using namespace std;

KlientProMQTT * KlientProMQTT::instance = nullptr;

KlientProMQTT * KlientProMQTT::ziskatInstanci(QQmlEngine * engine, QJSEngine * scriptEngine)
{
    Q_UNUSED(engine);
    Q_UNUSED(scriptEngine);
    
    if(KlientProMQTT::instance == nullptr)
    {
        KlientProMQTT::instance = new KlientProMQTT();
    }
    
    return KlientProMQTT::instance;
}

// konstrukotr skoro nic nedělá
// všecko bude nastavovaný jinejma metodama
KlientProMQTT::KlientProMQTT(QObject * parent) : QObject(parent), stav(NEPRIPOJENEJ), m_nazvyTopicu(nullptr), m_QoSsTopicu(nullptr)
{
}

// metoda stop poprosí vlákno vo to aby se jakože přešmiklo a počká
// void KlientProMQTT::stop()
// {
//     requestInterruption();
//     wait();
// }


// run se zkusí připojit a pak furt vesmyčce kouká jestli neni znova připojenej
// void KlientProMQTT::run()
// {
//     if(!pripojit())
//     {
//         qDebug()<<"nepovedlo se pripojit klienta";
//         return;
//     }
//     while(!isInterruptionRequested())
//     {
// 
//         usleep(CEKACI_CAS);
//         
//         if(stav == NEPRIPOJENEJ && jePripojenej())
//         {
//             stav = PRIPOJENEJ;
//             Q_EMIT pripojenej();
//         }
//     }
//     // když bylo vlákno přešmiknutý tak zkusíme vodpojit klienta
//     // jestli je připojenej si metoda vodpojit hlídá sama
//     odpojit();
//     
//     // qt::debugovací výstup
//     qDebug()<<"mqtt client konci!!!!!!!!!";
// }

// nastavování údajů na připojování k brokeroj
void KlientProMQTT::nastavit(const QString & adresa, const QString & id, const QString & uzivatel, const QString & heslo)
{
    // musíme převíst qstringy na vobyč std::stringy 
    // divný/neascii znkay se asi jako ztratěj tak pozor jako :O :O
    m_adresa = adresa.toStdString();
    m_id = id.toStdString();
    m_uzivatel = uzivatel.toStdString();
    m_heslo = heslo.toStdString();
    
    // inicianiluzujeme si ty struktury pro připojování/vodpojování/etc... výhozíma hodnotama
    m_connOpts = MQTTAsync_connectOptions_initializer;
    m_discOpts = MQTTAsync_disconnectOptions_initializer;
    m_sendRespOpts = MQTTAsync_responseOptions_initializer;
    m_subscribeRespOpts = MQTTAsync_responseOptions_initializer;
    
    // noa ty výhozí hodnoty jakože teťko přepišem nějakejma svejma kde potřebujem změnit
    
    // maximální možnej čas v sekundách kterej muže utýct vod posledního komunikování klienta s mqtt brokerem
    // když upline tak se pošle jakoby takovej 'ping' aby si navzájem řekli že sou voba eště naživu :O :O
    m_connOpts.keepAliveInterval = 20;
    
    // když máme zapnutý automatický znovupřipojování tak neni prej dobrý mit zapnutý promazávání session cache
    m_connOpts.cleansession = 0;
    
    //callbacky
    m_connOpts.onSuccess = KlientProMQTT::onConnect;
    m_connOpts.onFailure = KlientProMQTT::onConnectFail;
    
    // uživatel a heslo
    m_connOpts.username = m_uzivatel.c_str();
    m_connOpts.password = m_heslo.c_str();
    
    // zapnem automatický znovupřipojování a nastavíme znovupřipojovací interval ve kterým to jakoby tamto 
    // znovupřipojení zkouší třeba vod jedný do tří vteřin
    m_connOpts.automaticReconnect = 1;
    m_connOpts.minRetryInterval = 1;
    m_connOpts.maxRetryInterval = 3;
   
    // u vostatních struktur callbacky
    m_subscribeRespOpts.onSuccess = KlientProMQTT::onSubscribe;
    m_subscribeRespOpts.onFailure = KlientProMQTT::onSubscribeFail;
    
    m_sendRespOpts.onSuccess = KlientProMQTT::onSend;
    m_sendRespOpts.onFailure = KlientProMQTT::onSendFail;
    
    m_discOpts.onSuccess = KlientProMQTT::onDisconnect;
    m_discOpts.onFailure = KlientProMQTT::onDisconnectFail;
    
    // všecky tydlety struktury maj v sobě skovanej context/m_klienta
    // ho tam šoupnem až si ho v metodě připojit() vyrobíme ;D
}

// převedem vectory topiců/qos na ty ukazatelový pole. nejde to udělat nějak víc líp????? :O :O
// předpokádaj se stejně dlouhatánský pole aže se topicy nebudou vopakovat (to se hlídá v main.qml )
// neví někdo jestli de nějak v c++20 udělat todleto hele https://www.cplusplus.com/forum/general/228918/ :O :O :O :O
void KlientProMQTT::nastavitSeznamTopicu(const std::vector<QString> & topicy, const std::vector<int> & qos)
{
    if(topicy.size() != qos.size())
        throw runtime_error("neco je v qml moc spatne protoze do metody \'nastavitSeznamTopicu\' sou strceny vektory o ruzny dylce :O :O");

    if(topicy.size() == 0)
        qDebug()<<"do metody \'nastavitSeznamTopicu\' se strkaj vectory o nulovy dylce :O :O";
    
    
    if(m_nazvyTopicu != nullptr)
        delete [] m_nazvyTopicu;
    if(m_QoSsTopicu != nullptr)
        delete [] m_QoSsTopicu;
    
    m_pocetTopicu = topicy.size();
    m_nazvyTopicu = new char * [m_pocetTopicu];
    m_QoSsTopicu = new int [m_pocetTopicu];
    for(size_t i = 0; i < m_pocetTopicu; i++)
    {
        m_nazvyTopicu[i] = strdup(topicy[i].toStdString().c_str());
        m_QoSsTopicu[i] = qos[i];
        qDebug()<<"klient se bude chtit pripojovat k topicu \'"+QString(m_nazvyTopicu[i])+"\' s QoS == "<<m_QoSsTopicu[i];
    }
}


bool KlientProMQTT::pripojit()
{
    // návratovej chybvoej kód když se něco nepovede
    // mužem si pak dycky najít v hlavičkovým souboru 'MQTTAsync.h' tý paho knihovničky cože
    // to jako znamená tadleta chyba
    int rc; 
    
    // vyrobíme m_klienta 
    if ((rc = MQTTAsync_create(&m_klient, m_adresa.c_str(), m_id.c_str(), MQTTCLIENT_PERSISTENCE_NONE, nullptr))!= MQTTASYNC_SUCCESS)
    {
        string err = "nepodarilo se vyrobit MQTTAsync klienta!!!!!!!!!!!!\nnavratovej chybovej kod: ";
        err += to_string(rc);
        throw runtime_error(err);
    }

    if ((rc = MQTTAsync_setCallbacks(m_klient, m_klient, onConnLost, onMessage, onDeliver)) != MQTTASYNC_SUCCESS)
    {
        string err = "nepodarilo se klientoj nastavit callbacky!!!!!!!!!!!!\nnavratovej chybovej kod: ";
        err += to_string(rc);
        throw runtime_error(err);
    }
    
    // nastavíme callback volanej při připojení
    // méno tý funkce 'MQTTAsync_setConnected' je jakože dost zavádějicí takže sem si toho hnedka nevšimla :O :/
    if( (rc = MQTTAsync_setConnected(m_klient, m_klient, onConnected)) != MQTTASYNC_SUCCESS)
    {
        string err = "nepodarilo se klientoj nastavit on connected callback!!!!!!!!!!!!\nnavratovej chybovej kod: ";
        err += to_string(rc);
        throw runtime_error(err);        
    }
    
    // vyrobenýho klienta jakoby nastrkáme do těch nastavovacích struktur
    m_connOpts.context = m_klient;
    m_subscribeRespOpts.context = m_klient;
    m_sendRespOpts.context = m_klient;
    m_discOpts.context = m_klient;
    
    if((rc = MQTTAsync_connect(m_klient, &m_connOpts)) != MQTTASYNC_SUCCESS)
    {
        QString err = "nepodarilo se pripojit klienta!!!!!!!!!!!! navratovej chybovej kod: " + QString::number(rc);
        qDebug()<<err;
        Q_EMIT chyba("nepodarilo se pripojit klienta!!!!\nmrkni na prihlasovaci udaje");
        return false;
    }

    while(stav == NEPRIPOJENEJ)
        usleep(CEKACI_CAS);
    
    if(stav == CHYBA)
    {        
        QString err = "nepodarilo se subscribnout klienta!!!!!!";
        qDebug()<<err;
        Q_EMIT chyba(err);
        return false;
    }
    
    return true;
    
}

void KlientProMQTT::odpojit()
{
    // když neni připojenej tak ho nemužem začít vodpojovat
    // mqttasync.h nato nadává 
    if(!jePripojenej())
         return;
    int rc;
    if ((rc = MQTTAsync_disconnect(m_klient, &m_discOpts)) != MQTTASYNC_SUCCESS)
    {
        string err = "nepodarilo se zacit vodpojovani klienta!!!!!!!!!!!!\nnavratovej chybovej kod: " + to_string(rc);
        throw runtime_error(err);
    }
    // budem čekat než to jakoby nějak dopadne :D
    while (stav == PRIPOJENEJ && stav != CHYBA)
        usleep(CEKACI_CAS);
    
    // když to skončí chybou tak máme nějakej problémek kterej tady asi jako nevyřešíme
    if(stav == CHYBA)
    {        
        string err = "nepodarilo se vodpojit klienta!!!!!!!!!!!!";
        throw runtime_error(err);
    }
}

void KlientProMQTT::subscribe()
{
    if( stav != PRIPOJENEJ)
    {
        string err = "klient musi bejt pred subscribnutim pripojenej!!!!";
        throw runtime_error(err);
    }
    int rc;
    if ((rc = MQTTAsync_subscribeMany(m_klient, m_pocetTopicu, m_nazvyTopicu, m_QoSsTopicu, &m_subscribeRespOpts)) != MQTTASYNC_SUCCESS)
    {
        string err = "nepodarilo se zacit subscribovat klienta!!!!!!!!!!!!\nnavratovej chybovej kod: " + to_string(rc);
        throw runtime_error(err);
    }
}

void KlientProMQTT::poslat(const QString & topic, const QString & zprava, unsigned int qos, bool retained)
{
    // když neni klient připojenej nic se nebude posílat
    // posílání zpráviček z vodpojenýho klienta má jakoby zabránit zamikání čudliků v gui dycky když je klient vodpojenej
    if(!jePripojenej())
    {
        qDebug()<<"klient neni pripojenej takze se nic nebude posilat";
        return;
    }
    
    string zpravaStdstr = zprava.toStdString();
        
    MQTTAsync_message msg = MQTTAsync_message_initializer;
    msg.payload = (void *)zpravaStdstr.c_str();
    msg.payloadlen = zpravaStdstr.length();
    msg.qos = qos;
    msg.retained = int(retained);
        
    int rc;
    if ((rc = MQTTAsync_sendMessage(m_klient, topic.toStdString().c_str(), &msg, &m_sendRespOpts)) != MQTTASYNC_SUCCESS)
    {
        string err = "nepodarilo se poslat str zpravu!!!!!!!!!!!!\nnavratovej chybovej kod: " + to_string(rc);
        throw runtime_error(err);
    }
}

// vodpojíme nastavíme a zkusíme znova připojit
void KlientProMQTT::reset(const QString & adresa, const QString & id, const QString & uzivatel, const QString & heslo, const std::vector<QString> & topicy, const std::vector<int> & qos)
{
    odpojit();
    
    if(m_klient != nullptr)
        MQTTAsync_destroy(&m_klient);
    
    nastavit(adresa, id, uzivatel, heslo);
    nastavitSeznamTopicu(topicy, qos);
    
    stav = NEPRIPOJENEJ;
    pripojit();
}


//callbacky

void KlientProMQTT::onDisconnect(void * context, MQTTAsync_successData * odpoved)
{
    (void)context;
    (void)odpoved;
    instance->stav = NEPRIPOJENEJ;
    Q_EMIT instance->odpojenej();
    qDebug()<<"uspesne odpojeni"<<Qt::endl;
}
    
void KlientProMQTT::onDisconnectFail(void * context, MQTTAsync_failureData * odpoved)
{
    (void)context;
    instance->stav = CHYBA;
    Q_EMIT instance->chyba("neuspesne vodpojeni");
    qDebug()<<"neuspesne vodpojeni "<<Qt::endl<<"chybovy kod: "<<odpoved->code<<Qt::endl;
}
    
void KlientProMQTT::onSubscribe(void * context, MQTTAsync_successData * odpoved)
{
    (void)context;
    (void)odpoved;
    instance->stav = ZAPSANEJ;
    qDebug()<<"uspesne zapsani k topicu"<<Qt::endl;
}
    
void KlientProMQTT::onSubscribeFail(void * context, MQTTAsync_failureData * odpoved)
{
    (void)context;
    instance->stav = CHYBA;
    Q_EMIT instance->chyba("neuspesne zpasani k topicu");
    qDebug()<<"neuspesne zpasani k topicu"<<Qt::endl<<"chybovy kod: "<<odpoved->code<<Qt::endl;
}
    
void KlientProMQTT::onConnect(void * context, MQTTAsync_successData * odpoved)
{
    (void)context;
    (void)odpoved;
    qDebug()<<"uspesne pripojeno"<<Qt::endl;
    instance->stav = PRIPOJENEJ;
    Q_EMIT instance->pripojenej();
    instance->subscribe();
}
    
void KlientProMQTT::onConnectFail(void * context, MQTTAsync_failureData * odpoved)
{
    (void)context;
    qDebug()<<"neslo pripojit"<<Qt::endl<<"chybovy kod: "<<odpoved->code<<Qt::endl;
    instance->stav = CHYBA;
    Q_EMIT instance->chyba("neslo pripojit");
}
    
void KlientProMQTT::onConnected(void * context, char * duvod)
{
    (void)context;
    qDebug()<<"on connected callback";
    if(duvod != nullptr)
        qDebug()<<"duvod: "<<duvod;
    instance->stav = PRIPOJENEJ;
    Q_EMIT instance->pripojenej();
    //instance->subscribe();
}

void KlientProMQTT::onSend(void * context, MQTTAsync_successData * odpoved)
{
    (void)context;
    (void)odpoved;
    qDebug()<<"poslalo se"<<Qt::endl;
}
    
void KlientProMQTT::onSendFail(void * context, MQTTAsync_failureData * odpoved)
{
    (void)context;
    qDebug()<<"nepodarilo se odeslat ZPRAVU"<<Qt::endl<<"chybovy kod: "<<odpoved->code<<Qt::endl;
    qDebug()<<"klient se tedko pokusi vodpojit";
    Q_EMIT instance->chyba("neslo poslat zpravu");
    instance->odpojit();
}

void KlientProMQTT::onDeliver(void * context, MQTTAsync_token token)
{
    (void)context;
    qDebug()<<"doruceni zpravy potvrzeno"<<Qt::endl<<"token: "<<token<<Qt::endl<<Qt::endl;
}
    
void KlientProMQTT::onConnLost(void * context, char * duvod)
{
    (void)context;
    instance->stav = NEPRIPOJENEJ;
    Q_EMIT instance->odpojenej();
    qDebug()<<"spojeni ztraceno";
    if(duvod != nullptr)
        qDebug()<<"duvod odpojeni: "<<duvod;
}
    
//vrací jedničku když zprávu správně přijmem jinak nám ji to zkusí znova strčit
int KlientProMQTT::onMessage(void * context, char * topicNazevStr, int topicStrZnaku, MQTTAsync_message * zprava)
{
    (void)context;
    (void)topicStrZnaku;
            
    if(zprava->payloadlen > 0)
    {
        qDebug()<<"prijata zprava do topicu \'" + QString(topicNazevStr) +"\' s vobsahem:";
        qDebug()<<QString::fromLocal8Bit((char *)zprava->payload);
        
        QString topicQstr = QString::fromLocal8Bit(topicNazevStr), zpravaQstr = QString::fromLocal8Bit((char *)zprava->payload);
                
        if( ((char *)zprava->payload)[0] == '{' )
        {
            cJSON * jsonZprava = cJSON_Parse((char*)zprava->payload);
            if (jsonZprava != nullptr)
            {
                //zprava asi jako bude ve formatu json
                cJSON_Delete(jsonZprava);
                        
                Q_EMIT instance->zpravaJsonDoFrontendu(topicQstr, zpravaQstr);
                        
                MQTTAsync_freeMessage(&zprava);
                MQTTAsync_free(topicNazevStr);
                return 1;
            }
        }
                
        Q_EMIT instance->zpravaDoFrontendu(topicQstr, zpravaQstr);
    }
    else
        qDebug()<<"prijata prazdna zprava :O :O";
            
    MQTTAsync_freeMessage(&zprava);
    MQTTAsync_free(topicNazevStr);
    return 1;
}
    
    
// destruktor co dělá že uklidí
KlientProMQTT::~KlientProMQTT()
{
    if(jePripojenej())
        odpojit();
    
    if(m_klient != nullptr)
        MQTTAsync_destroy(&m_klient);
    
    delete [] this->m_nazvyTopicu;
    delete [] this->m_QoSsTopicu;
}
