#include "PlasmoidPlugin.h"
#include "KlientProMQTT.h"
#include "NacitadloSouboru.h"

#include <QtQml>
#include <QDebug>

void PlasmoidPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("org.kde.private.mqttCudliky"));
    
    qmlRegisterSingletonType<KlientProMQTT>(uri, 1, 0, "Klient", KlientProMQTT::ziskatInstanci);
    qmlRegisterType<NacitadloSouboru>(uri, 1, 0, "Nacitadlo");
}
