#ifndef KLIENT_PRO_MQTT_H
#define KLIENT_PRO_MQTT_H

#include <MQTTAsync.h>
#include <cjson/cJSON.h>

#include <string.h>
#include <unistd.h>

#include <string>
#include <map>

#include <qt5/QtCore/QDebug>
#include <qt5/QtCore/QObject>
#include <qt5/QtCore/QThread>
#include <qt5/QtCore/QString>
#include <qt5/QtQml/QJSEngine>
#include <qt5/QtQml/QQmlEngine>

#define CEKACI_CAS 10000L
#define VYCHOZI_QOS 0

//jakej je jako stav připojení  
enum StavPripojeni { CHYBA, NEPRIPOJENEJ, PRIPOJENEJ, ZAPSANEJ };

// podědíme z 
class KlientProMQTT : public QObject
{
    // qt oběkty to tady ve třídě musej mit :O :O
    Q_OBJECT
    
public:
    
    // jeto singleton/jedináček takže mužem mit jenom jednu instanci třídy vyrobenou
    // skovanou v nějaký statický proměný třídy
    // konstrukor je private a misto něj se volá nějaká statická metoda co vrací tu instanci společnou
    // singleton budem pak registrovat ve třídě 'PlasmoidPlugin' jako qml typ funckí 'qmlRegisterSingletonType'
    // ata chce aby metoda getInstance hamala tydlety ukazatele ;D
    static KlientProMQTT * ziskatInstanci(QQmlEngine * engine,  QJSEngine * scriptEngine);
    
    
    //void poslat(const QString & topic, const QString & zprava, unsigned int qos, bool retained);
    
    //StavPripojeni getStav() const {return stav;}

    // destruktor co dělá že uklidí
    ~KlientProMQTT();
    
signals:
    
    // signály který qt oběkt bude posílat
    // v qml nato sou napojený chytací metody
    // definujem jakoby jenom hlavičky metod žádný tělíčka. všecko co se do tý signal metody narve
    // se pak strčí do argumentu na stejný pozici vtěch qml/js funckí napojenejch
    
    // v c++/qt zdrojáčku se pak před nima musí psát 'Q_EMIT' abyto fungovalo :O :O
    
    // signály co dělaj že předaj klientem chycenou zprávu do qml frontendu
    void zpravaDoFrontendu(const QString & topic, const QString & obsahZpravy);
    void zpravaJsonDoFrontendu(const QString & topic, const QString & obsahZpravy);
    
    // signály který mqtt klient posílá když se vodpojí/připojí/něco se rozbije
    void odpojenej();
    void pripojenej();
    void chyba(QString textChyby = "chyba :D");
    
public Q_SLOTS:
    
    // funkce co připojujou a vodpojujou k/vod mqtt brokera
    // funkce připojit při úspěchu vrací true a při asi závažnějších chybách blije vyjímky
    bool pripojit();
    void odpojit();
    
    // nastaví atributy m_adresa m_id a tydlety všecky který potřebujem k připojování k brokeroj a vyrobí podle nich 
    // struktury m_connOpts, m_discOpts...... který budem nastrkávat do funckí z knihovničky MQTTAsync.h 
    void nastavit(const QString & adresa, const QString & id, const QString & uzivatel, const QString & heslo);
    
    // nastaví seznam topiců + jejich qos/kvalitu služby z qml/javascriptovejch polí strčenejch sem
    // každej topic by tam měl bejt jenom jednou napsanej
    // nemužem použít QMap nebo nějakej jinej víc chytřejší vobal protože naněco takovýho neumí qml převíst :O :/
    // převody mezi qml a qt/c++ maj popsaný tady hele https://doc.qt.io/qt-5/qtqml-cppintegration-data.html
    void nastavitSeznamTopicu(const std::vector<QString> & topicy, const std::vector<int> & qos);
    
    // pošlem z klienta zprávu
    // žere to jakoby ty qstringy ale ty sou převáděný na vobyč str::stringy uvnitř takže byse dotoho němeli strkat
    // žádný víc speciální znaky protože byje to asi jako nemuselo převíst :O :O
    // mqtt topicy by asi jako taky měli bejt složený jenom s vobyč ascii znáčků si myslim :O :O
    void poslat(const QString & topic, const QString & obsahZpravy, unsigned int qos = VYCHOZI_QOS, bool retained = false);
    
    // odpojí klienta nastaví adresu id etc + topicy/qos a zkusí znova udělat připojení
    void reset(const QString & adresa, const QString & id, const QString & uzivatel, const QString & heslo, const std::vector<QString> & topicy, const std::vector<int> & qos);
    
    // metoda co řiká jestli je mqtt klient připojenej
    bool jePripojenej() const {return (bool)MQTTAsync_isConnected(m_klient);}
    
private:
    
    // tajenj privátní konstruktor takže muže bejt zavolanej jenom z nějaký jiný metody týdletý třídy
    // singleton se nám vyrábí jenom  v metodě 'ziskatInstanci'
    explicit KlientProMQTT(QObject * parent = nullptr);
    
    // tady je skovaná ta jediná instance týdletý třídy
    static KlientProMQTT * instance;

    // metoda na zapsání k topicu 
    // před zapsáním nejdřiv musíme bejt připojený k brokeroj bezchybně
    void subscribe();
    
    StavPripojeni stav;
    
    // přihlašovací údaje/data pro mqtt brokera
    std::string m_adresa;
    std::string m_id;
    std::string m_uzivatel;
    std::string m_heslo;
    
    // ke kolika topiců se chcem zapsat a seznam jejich názvů + QoS(kvalita služby)
    // mqtt vobyč cčková knihova chce vobyč starý cčkový pole/ukazate navstup
    size_t m_pocetTopicu;
    char ** m_nazvyTopicu;
    int * m_QoSsTopicu;
    
    // mqttasync.h knihovnou vyrobenej klient
    // s nim pracujou všecky ty funkce ztý knihovy
    MQTTAsync m_klient;
    
    // různý struktury nastavovací při připojování/vodpojování m_klienta
    // strkaj se do nich různý parametry + věčinou dvojice ukazatelů na funcke který sou pak
    // zavolaný jako callbacky potom cose to povede/nepovede udělat
    
    // připojovací
    MQTTAsync_connectOptions m_connOpts;
    
    // vodpojovací
    MQTTAsync_disconnectOptions m_discOpts;
    
    //na posílání zpráv
    MQTTAsync_responseOptions m_sendRespOpts;
    
    // subscribovací/na zapisování do topiců
    MQTTAsync_responseOptions m_subscribeRespOpts;
    
    // a tady sou ty různý callbacky jako statický metody
    // vobyčejný metody voběktu by to neumělo zhamat protože ty maj v c++ prej
    // jako takovej první neviditelnej argument ukazatel na sám sebe voběkt nebo takovýho něco
    // context je vodkaz na mqtt klienta připojenýho kterýho si musíme přetypovávat z void když ho třeba potřebujem
    
    // callback při vodpojení úspěch/neůspěch
    static void onDisconnect(void * context, MQTTAsync_successData * odpoved);
    static void onDisconnectFail(void * context, MQTTAsync_failureData * odpoved);
    
    // callbacky pro zapisování k topicům
    static void onSubscribe(void * context, MQTTAsync_successData * odpoved);
    static void onSubscribeFail(void * context, MQTTAsync_failureData * odpoved);
    
    // připojování
    // onConnect callbacky nejsou volaný při automatickým znovupřipojení klienta
    static void onConnect(void * context, MQTTAsync_successData * odpoved);
    static void onConnectFail(void * context, MQTTAsync_failureData * odpoved);
    
    static void onConnected(void * context, char * cause);
    
    // callbacky pro posílání zpráviček klientem
    static void onSend(void * context, MQTTAsync_successData * odpoved);
    static void onSendFail(void * context, MQTTAsync_failureData * odpoved);

    // při doručení zprávy
    static void onDeliver(void * context, MQTTAsync_token token);
    
    // ztráta spojení
    static void onConnLost(void * context, char * duvod);
    
    // při přijetí zprávy
    // pozor je tam hulvátstyle kontrola jestli je zpráva string/řetězec ve formátu json že
    // se kouká na první znak jestli toje složená závorka '{'
    static int onMessage(void * context, char * topicNazevStr, int topicStrZnaku, MQTTAsync_message * zprava);
    
    
};



#endif
