#include "NacitadloSouboru.h"

#include <stdio.h>
#include <stdlib.h>

QString NacitadloSouboru::nacistSoubor(const QUrl & cesta)
{    
        QFile soubor(cesta.toLocalFile());
        
        if(!soubor.open(QFile::ReadOnly | QFile::Text))
            return "neslo otevrit!!!!!!!!!!";
        else
            return soubor.readAll();
        
        
}
