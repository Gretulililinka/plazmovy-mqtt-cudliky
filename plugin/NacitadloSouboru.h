#ifndef NACITADLO_SOUBORU_H
#define NACITADLO_SOUBORU_H

#include <qt5/QtCore/QObject>
#include <qt5/QtCore/QUrl>
#include <qt5/QtCore/QFile>
#include <qt5/QtCore/QDebug>

class NacitadloSouboru : public QObject
{
    Q_OBJECT
    
public:
    
    explicit NacitadloSouboru(QObject * parent = nullptr) : QObject(parent){}
    ~NacitadloSouboru(){}
    
    Q_SLOT QString nacistSoubor(const QUrl & cesta);
};


#endif
